<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="container header">
	<div class="span5">
		<div class="logo">
			<a href="${pageContext.request.contextPath}/index.action"> <img
				src="${pageContext.request.contextPath}/image/r___________renleipic_01/logo.jpg"
				alt="罗小姐" />
			</a>
		</div>
	</div>
	<div class="span9">
		<div class="headerAd">
			<img src="${pageContext.request.contextPath}/image/header.jpg"
				width="320" height="50" alt="正品保障" title="正品保障" />
		</div>
	</div>

	<div class="span10 last">
		<div class="topNav clearfix">
			<ul>
				<c:choose>
					<c:when test="${empty sessionScope.sessionUser }">
						<li id="headerLogin" class="headerLogin"
							style="display: list-item;"><a
							href="${pageContext.request.contextPath }/user_loginPage.action">登录</a>|
						</li>
						<li id="headerRegister" class="headerRegister"
							style="display: list-item;"><a
							href="${pageContext.request.contextPath }/user_registPage.action">注册</a>|
						</li>
					</c:when>
					<c:otherwise>
						<li id="headerUsername" class="headerUsername"
							style="display: list-item;">${sessionScope.sessionUser.name }</li>
						<li id="headerLogout" class="headerLogout"
							style="display: list-item;"><a
							href="${pageContext.request.contextPath }/user_exit.action">[退出]</a>|</li>
						<li id="headerMyOrder" class="headerMyOrder"
							style="display: list-item;"><a
							href="${pageContext.request.contextPath }/order_myOrder.action">我的订单</a>|
						</li>
					</c:otherwise>
				</c:choose>
				<li><a>会员中心</a> |</li>
				<li><a>购物指南</a> |</li>
				<li><a>关于我们</a></li>
			</ul>
		</div>
		<div class="cart">
			<a href="${pageContext.request.contextPath }/cart_findByUid.action">购物车</a>
		</div>
		<div class="phone">
			客服热线: <strong>18580767267</strong>
		</div>
	</div>
	<div class="span24">
		<ul class="mainNav">
			<li><a href="${pageContext.request.contextPath }/index.action">首页</a>
				|</li>
			<s:iterator value="#session.cList" var="category">
				<li><a
					href="${pageContext.request.contextPath }/product_findByCid.action?cid=<s:property value="#category.cid" />&page=1"><s:property
							value="#category.cname" /></a> |</li>
			</s:iterator>
		</ul>
	</div>
</div>