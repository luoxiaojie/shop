<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>创建订单成功</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/ordersucc.css">
<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />

</head>

<body>

	<%@ include file="menu.jsp"%>
	<div class="container cart">
		<div class="span24">
			<div class="step step1"></div>
			<div class="div1">
				&nbsp;&nbsp;&nbsp;&nbsp;
				<font color="red" size="6">订单已生成</font>
			</div>
			<div class="div2">
				<img src="<c:url value='/images/duihao.jpg'/>" class="img" />
				<dl>
					<dt>下单时间：&nbsp;<s:date name="model.ordertime" /></dt>
					
					<dt>订单编号: &nbsp;<s:property value="model.oid"/></dt>
					<dt>合计金额:</dt>
					<dd>
						<span class="price_t">&yen;<s:property value="model.total"/></span>
					</dd>
					<dt>收货人:&nbsp;<s:property value="model.name"/></dt>
					<dt>联系电话:&nbsp;<s:property value="model.phone"/></dt>
					<dt>收货地址:&nbsp;<s:property value="model.addr"/></dt>
				</dl>
				<br/>
				<span>感谢您的支持，祝您购物愉快！</span> <a
					href="${pageContext.request.contextPath }/order_payOrder?oid=<s:property value="model.oid" />"
					id="linkPay">支付</a>
			</div>
		</div>
	</div>

</body>
</html>
