<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>信息板</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<style type="text/css">
body {
	font-size: 10pt;
	color: #404040;
	font-family: SimSun;
}

.divBody {
	margin-left: 15%;
}

.divTitle {
	text-align: left;
	width: 900px;
	height: 25px;
	line-height: 25px;
	background-color: #efeae5;
	border: 5px solid #efeae5;
}

.divContent {
	width: 900px;
	height: 230px;
	border: 5px solid #efeae5;
	margin-right: 20px;
	margin-bottom: 20px;
}

.spanTitle {
	margin-top: 10px;
	margin-left: 10px;
	height: 25px;
	font-weight: 900;
}

a {
	text-decoration: none;
}

a:visited {
	color: #018BD3;
}

a:hover {
	color: #FF6600;
	text-decoration: underline;
}
}
</style>

</head>

<body>
	<%@ include file="menu.jsp"%>
	<c:choose>
		<c:when test="${code eq 'success' }">
			<c:set var="img" value="/images/duihao.jpg" />
			<c:set var="title" value="成功" />
		</c:when>
		<c:when test="${code eq 'error' }">
			<c:set var="img" value="/images/cuohao.png" />
			<c:set var="title" value="失败" />
		</c:when>

	</c:choose>
	<div class="divBody">
		<div class="divTitle">
			<span class="spanTitle">${title }</span>
		</div>
		<div class="divContent">
			<div style="margin: 20px;">
				<img style="float: left; margin-right: 30px;"
					src="<c:url value='${img }'/>" width="150" /> <span
					style="font-size: 30px; color: #c30; font-weight: 900;">${msg }</span>
				<br /> <br /> <br /> <br />
				<c:if test="${empty sessionScope.sessionUser }">
					<span style="margin-left: 50px;"><a target="_top"
						href="<c:url value='/user_loginPage.action'/>">登录</a></span>
				</c:if>
				<span style="margin-left: 50px;"><a target="_top"
					href="<c:url value='/index.action'/>">主页</a></span>
				<c:if test="${! empty mailAddress }">
					<span style="margin-left: 50px;"><a target="_top"
						href="http://${mailAddress }">点击这里登陆邮箱</a></span>
				</c:if>
			</div>
		</div>
	</div>
	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="${pageContext.request.contextPath}/image/footer.jpg"
					width="950" height="52" alt="我们的优势" title="我们的优势" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a>关于我们</a> |</li>
				<li><a>联系我们</a> |</li>
				<li><a>招贤纳士</a> |</li>
				<li><a>法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>服务声明</a> |</li>
				<li><a>广告声明</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright © 2005-2015 网上商城 版权所有</div>
		</div>
	</div>

</body>
</html>
