<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>购物车</title>

<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/cart.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/js/round.js"></script>
<script type="text/javascript">
	$(function() {
		var total = 0;
		$(".subtotal").each(function() {
			var id = $(this).attr("id");//得到了cartItemId值
			var text = $("#" + id).text();//通过前缀获取小计元素
			total += Number(text);
		});
		total = round(total, 2);
		$("#effectivePrice").text(total);
		$("#effectivePoint").text(total);

	});
</script>

</head>
<body>
	<%@ include file="menu.jsp"%>

	<s:if test="listCartItem.isEmpty()">
		<div class="container cart">
			<div class="span24">
				<br /> <br /> <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font
					color="red" size="6">亲，购物车还没有东西哟，赶快去购物吧！</font> <br /> <br /> <br />
				<br /> <br /> <br /> <br />
			</div>
		</div>
	</s:if>
	<s:else>
		<div class="container cart">
			<div class="span24">
				<div class="step step1"></div>
				<table>
					<tbody>
						<tr>
							<th>图片</th>
							<th>商品</th>
							<th>价格</th>
							<th>数量</th>
							<th>小计</th>
							<th>操作</th>
						</tr>
						<s:iterator value="listCartItem" var="cartItem">
							<tr>
								<td width="60"><input type="hidden" name="id" value="22" />
									<img
									src="${pageContext.request.contextPath}/<s:property value="#cartItem.image"/>" /></td>
								<td><a
									href="${pageContext.request.contextPath}/product_findByPid.action?pid=<s:property value="#cartItem.pid"/>"
									target="_blank"><s:property value="#cartItem.pname" /></a></td>
								<td>￥<s:property value="#cartItem.shop_price" /></td>
								<td class="quantity" width="60"><s:property
										value="#cartItem.quantity" /></td>
								<td width="140">￥<span
									id="<s:property value="#cartItem.cartItemId"/>Subtotal"
									class="subtotal"><s:property value="#cartItem.subtotal" /></span></td>
								<td><a
									href="${pageContext.request.contextPath}/cart_removeCartItemByPid.action?pid=<s:property value="#cartItem.pid"/>"
									class="delete">删除</a></td>
							</tr>
						</s:iterator>
					</tbody>
				</table>
				<div class="total">
					<em id="promotion"></em>赠送积分: <em class="effectivePoint"
						id="effectivePoint"></em> 商品金额: ￥<strong class="effectivePrice"
						id="effectivePrice"></strong>元
				</div>
				<div class="bottom">
					<a href="${pageContext.request.contextPath}/cart_clearCart.action"
						id="clear" class="clear">清空购物车</a>
					<a href="${pageContext.request.contextPath}/cart_submitOrder.action"
						id="submit" class="submit">提交订单</a>
				</div>
			</div>
		</div>
	</s:else>

	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="${pageContext.request.contextPath}/image/footer.jpg"
					width="950" height="52" alt="我们的优势" title="我们的优势" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a>关于我们</a> |</li>
				<li><a>联系我们</a> |</li>
				<li><a>招贤纳士</a> |</li>
				<li><a>法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>服务声明</a> |</li>
				<li><a>广告声明</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright © 2005-2015 网上商城 版权所有</div>
		</div>
	</div>
</body>
</html>