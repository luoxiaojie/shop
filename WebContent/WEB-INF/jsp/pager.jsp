<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">

	function _go() {
		var page = $("#pageCode").val();//获取文本框中的当前页码
		if(!/^[1-9]\d*$/.test(page)) {//对当前页码进行整数校验
			alert('请输入正确的页码！');
			return;
		}
		if(page > ${pageBean.totalPage}) {  //判断当前页码是否大于最大页
			alert('请输入正确的页码！');
			return;
		}
		location = "${pageBean.url}&page="+page;
	}

</script>


<div class="divBody">
	<div class="divContent">
		<s:if test="pageBean.page < 2">
			<span class="spanBtnDisabled">上一页</span>
		</s:if>
		<s:else>
			<a
				href="<s:property value="pageBean.url"/>&page=<s:property value="pageBean.page -1"/>"
				class="aBtn bold">上一页</a>
		</s:else>

		<c:choose>
			<c:when test="${pageBean.totalPage <= 6 }">
				<c:set var="begin" value="1" />
				<c:set var="end" value="${pageBean.totalPage }" />
			</c:when>
			<c:otherwise>
				<c:set var="begin" value="${pageBean.page-2 }" />
				<c:set var="end" value="${pageBean.page+3 }" />
				<c:if test="${begin < 1 }">
					<c:set var="begin" value="${1 }" />
					<c:set var="end" value="${6 }" />
				</c:if>
				<c:if test="${end > pageBean.totalPage }">
					<c:set var="begin" value="${pageBean.totalPage-5 }" />
					<c:set var="end" value="${pageBean.totalPage }" />
				</c:if>
			</c:otherwise>
		</c:choose>

		<%-- 显示页码列表 --%>
		<c:forEach begin="${begin }" end="${end }" var="i">
			<c:choose>
				<c:when test="${i eq pageBean.page }">
					<span class="spanBtnSelect">${i }</span>
				</c:when>
				<c:otherwise>
					<a
						href="<s:property value="pageBean.url"/>&page=${i }"
						class="aBtn">${i }</a>
				</c:otherwise>
			</c:choose>
		</c:forEach>
		<%-- 显示点点点 --%>
		<c:if test="${end < pageBean.totalPage }">
			<span class="spanApostrophe">...</span>
		</c:if>



		<s:if test="pageBean.page > pageBean.totalPage-1">
			<span class="spanBtnDisabled">下一页</span>
		</s:if>
		<s:else>
			<a
				href="<s:property value="pageBean.url"/>&page=<s:property value="pageBean.page +1"/>"
				class="aBtn bold">下一页</a>
		</s:else>

		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<%-- 共N页 到M页 --%>
		<span>共${pageBean.totalPage }页</span> 
		<span>到</span> 
		<input type="text" size="1px" class="inputPageCode" id="pageCode" value="${pageBean.page }" /> 
		<span>页</span>
		<a href="javascript:_go();" class="aSubmit">确定</a>
	</div>
</div>