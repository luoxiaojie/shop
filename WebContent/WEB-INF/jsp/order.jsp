<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0043)http://localhost:8080/mango/cart/list.jhtml -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>订单页面</title>
<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/cart.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script src="${pageContext.request.contextPath}/js/round.js"></script>
<script type="text/javascript">
	$(function() {
		var total = 0;
		$(".subtotal").each(function() {
			var id = $(this).attr("id");//得到了cartItemId值
			var text = $("#" + id).text();//通过前缀获取小计元素
			total += Number(text);
		});
		total = round(total, 2);
		$("#effectivePrice").text(total);
		$("#effectivePoint").text(total);

		$("#orderForm").submit(function() {
			var cartItemIdArray = new Array();
			$(".id").each(function() {
				var id = $(this).val();//得到了cartItemId值
				cartItemIdArray.push(id);
			});
			var cartItemIds = cartItemIdArray.toString();
			$("#cartItemIds").val(cartItemIds);
			$("#total").val(total);
			return true;
		});
	});
</script>
</head>
<body>

	<%@ include file="menu.jsp"%>
	<div class="container cart">
		<div class="span24">
			<div class="step step1"></div>
			<table>
				<tbody>

					<tr>
						<th>图片</th>
						<th>商品</th>
						<th>价格</th>
						<th>数量</th>
						<th>小计</th>
					</tr>
					<s:iterator value="listCartItem" var="cartItem">
						<tr>
							<td width="60"><input type="hidden" id="id" class="id"
								value="<s:property value="#cartItem.cartItemId"/>" /> <img
								src="${pageContext.request.contextPath}/<s:property value="#cartItem.image"/>" /></td>
							<td><a
								href="${pageContext.request.contextPath}/product_findByPid.action?pid=<s:property value="#cartItem.pid"/>"
								target="_blank"><s:property value="#cartItem.pname" /></a></td>
							<td>￥<s:property value="#cartItem.shop_price" /></td>
							<td class="quantity" width="60"><s:property
									value="#cartItem.quantity" /></td>
							<td width="140">￥<span
								id="<s:property value="#cartItem.cartItemId"/>Subtotal"
								class="subtotal"><s:property value="#cartItem.subtotal" /></span></td>
						</tr>
					</s:iterator>
				</tbody>
			</table>
			<dl id="giftItems" class="hidden" style="display: none;">
			</dl>
			<div class="total">
				<em id="promotion"></em>赠送积分: <em class="effectivePoint"
					id="effectivePoint"></em> 商品金额: ￥<strong class="effectivePrice"
					id="effectivePrice"></strong>元
			</div>
			<form id="orderForm"
				action="${pageContext.request.contextPath}/order_createOrder.action"
				method="post">
				<input type="hidden" name="cartItemIds" id="cartItemIds" /> <input
					type="hidden" name="total" id="total" />
				<div class="span24">
					<p>
						收货地址：<input name="addr" type="text"
							value="<s:property value="#session.sessionUser.addr"/>"
							style="width: 350px" /> <br /> 收货人&nbsp;&nbsp;&nbsp;：<input
							name="name" type="text"
							value="<s:property value="#session.sessionUser.name"/>"
							style="width: 150px" /> <br /> 联系方式：<input name="phone"
							type="text"
							value="<s:property value="#session.sessionUser.phone"/>"
							style="width: 150px" />

					</p>
					<hr />
					<p style="text-align: right">
						<input type="image"
							src="${pageContext.request.contextPath}/images/finalbutton.gif" />
					</p>
				</div>
			</form>
		</div>

	</div>
	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="image\r___________renleipic_01/footer.jpg" alt="我们的优势"
					title="我们的优势" height="52" width="950" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a href="#">关于我们</a> |</li>
				<li><a href="#">联系我们</a> |</li>
				<li><a href="#">诚聘英才</a> |</li>
				<li><a href="#">法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>SHOP++官网</a> |</li>
				<li><a>SHOP++论坛</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright © 2005-2015 网上商城 版权所有</div>
		</div>
	</div>
</body>
</html>