<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0048)http://localhost:8080/mango/product/list/1.jhtml -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>罗小姐的购物商城</title>
<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/product.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/pager.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
</head>
<body>
	<%@ include file="menu.jsp"%>

	<div class="container productList">
		<div class="span6">
			<div class="hotProductCategory">

				<s:iterator value="#session.cList" var="category">
					<dl>
						<dt>
							<a
								href="${pageContext.request.contextPath }/product_findByCid.action?cid=<s:property value="#category.cid" />&page=1"><s:property
									value="#category.cname" /></a>
						</dt>
						<s:iterator value="#category.setCategorySecond"
							var="categorySecond">
							<dd>
								<a
									href="${pageContext.request.contextPath }/product_findByCsid.action?csid=<s:property value="#categorySecond.csid" />&page=1"><s:property
										value="#categorySecond.csname" /></a>
							</dd>
						</s:iterator>
					</dl>
				</s:iterator>
			</div>
		</div>

		<div class="span18 last">
		
			<form id="productForm" action="" method="get">
				<div id="result" class="result table clearfix">

					<s:iterator value="pageBean.list" var="product">
						<ul>
							<li><a
								href="${pageContext.request.contextPath}/product_findByPid.action?pid=<s:property value="#product.pid"/>">
									<img
									src="${pageContext.request.contextPath}/<s:property value="#product.image"/>"
									width="170" height="170" style="display: inline-block;" /> <span
									style='color: green'> <s:property value="#product.pname" /></span>
									<span class="price"> 商城价： ￥<s:property
											value="#product.shop_price" />/件
								</span>

							</a></li>
						</ul>
					</s:iterator>
				</div>
				<div style="float: left; width: 100%; text-align: center;">
					<hr />
					<br />
					<%@include file="pager.jsp"%>
				</div>

			</form>
		</div>
	</div>


	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="${pageContext.request.contextPath}/image/footer.jpg"
					width="950" height="52" alt="我们的优势" title="我们的优势" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a>关于我们</a> |</li>
				<li><a>联系我们</a> |</li>
				<li><a>诚聘英才</a> |</li>
				<li><a>法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>官网</a> |</li>
				<li><a>论坛</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright©2005-2015 网上商城 版权所有</div>
		</div>
	</div>
</body>
</html>