<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>订单列表</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="content-type" content="text/html;charset=utf-8">

<link href="${pageContext.request.contextPath}/css/list.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/product.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/pager.css"
	rel="stylesheet" type="text/css" />

<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/laydate/laydate.js"></script>

</head>

<body>

	<%@ include file="menu.jsp"%>
	<div class="container productList">
		<div class="span6">
			<div class="hotProductCategory">

				<s:iterator value="#session.cList" var="category">
					<dl>
						<dt>
							<a
								href="${pageContext.request.contextPath }/product_findByCid.action?page=1&cid=<s:property value="#category.cid" />"><s:property
									value="#category.cname" /></a>
						</dt>
						<s:iterator value="#category.setCategorySecond"
							var="categorySecond">
							<dd>
								<a
									href="${pageContext.request.contextPath }/product_findByCsid.action?page=1&csid=<s:property value="#categorySecond.csid" />"><s:property
										value="#categorySecond.csname" /></a>
							</dd>
						</s:iterator>
					</dl>
				</s:iterator>
			</div>
		</div>

		<div class="span18 last">
		<br/>
			<center>
				<input class="laydate-icon" onclick="laydate()" id="demo"/>
				<input type="button" value="查询" id="sure"/>
			</center>	
			<div style="color: red;">
				<c:choose>
					<c:when test="${empty requestScope.year }">
					</c:when>
					<c:otherwise>
						${requestScope.year }年${requestScope.month }月消费总额为：${requestScope.totalMoney }
					</c:otherwise>
				</c:choose>
			</div>
			<div class="divTitle">
				<span style="margin-left: 150px; margin-right: 280px;">商品信息</span> <span
					style="margin-left: 10px; margin-right: 20px;">金额</span> <span
					style="margin-left: 30px; margin-right: 10px;">订单状态</span> <span
					style="margin-left: 35px; margin-right: 10px;">操作</span>
			</div>
			<br />
			<table align="center" border="0" width="100%" cellpadding="0"
				cellspacing="0">

				<s:iterator value="pageBean.list" var="order">
					<tr class="tt">
						<td width="330px">订单号：<a
							href="${pageContext.request.contextPath }/order_findByOid?oid=<s:property value="#order.oid"/>"><s:property
									value="#order.oid" /></a></td>
						<td width="330px">下单时间：<s:date name="#order.ordertime" /></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr style="padding-top: 10px; padding-bottom: 10px;">
						<td colspan="2"><s:iterator value="#order.setOrderItem"
								var="orderItem">
								<a class="link2"
									href="${pageContext.request.contextPath }/product_findByPid?Pid=<s:property value="#orderItem.product.pid"/>">
									<img border="0" width="70"
									src="${pageContext.request.contextPath }/<s:property value="#orderItem.product.image"/>" />
								</a>
							</s:iterator></td>
						<td width="130px"><span class="price_t">&yen;<s:property
									value="#order.total" /></span></td>
						<td width="190px"><s:if test="#order.state == 1">
								<font color="red">(等待付款)&nbsp;</font>
							</s:if> <s:if test="#order.state == 2">
								<font color="red">(准备发货)&nbsp;</font>
							</s:if> <s:if test="#order.state == 3">
								<font color="red">(等待收货)&nbsp;</font>
							</s:if> <s:if test="#order.state == 4">
								<font color="red">(交易成功)&nbsp;</font>
							</s:if> <s:if test="#order.state == 5">
								<font color="red">(已取消)&nbsp;</font>
							</s:if></td>
						<td width="50px"><a
							href="${pageContext.request.contextPath }/order_findByOid?oid=<s:property value="#order.oid"/>">查看</a><br />
							<s:if test="#order.state == 1">
								<a
									href="${pageContext.request.contextPath }/order_findByOid?oid=<s:property value="#order.oid"/>">支付</a>
								<br />
								<a
									href="${pageContext.request.contextPath }/order_findByOid?oid=<s:property value="#order.oid"/>">取消</a>
								<br />
							</s:if> <s:if test="#order.state == 3">
								<a
									href="${pageContext.request.contextPath }/order_findByOid?oid=<s:property value="#order.oid"/>">收货</a>
								<br />
							</s:if></td>
					</tr>
				</s:iterator>
			</table>
			<br />
			<div style="float: left; width: 100%; text-align: center;">
				<hr />
				<br />
				<%@include file="pager.jsp"%>
			</div>
		</div>

<script type="text/javascript">
	laydate({
		elem:'#demo'
	});
	$("#sure").click(function(){
		var date = $("#demo").val();
		window.location.href = "order_getMonthZD.action?zddate="+date;
	});
</script>


	</div>


	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="${pageContext.request.contextPath}/image/footer.jpg"
					width="950" height="52" alt="我们的优势" title="我们的优势" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a>关于我们</a> |</li>
				<li><a>联系我们</a> |</li>
				<li><a>诚聘英才</a> |</li>
				<li><a>法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>官网</a> |</li>
				<li><a>论坛</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright©2005-2015 网上商城 版权所有</div>
		</div>
	</div>
</body>
</html>
