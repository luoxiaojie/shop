<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>订单详细</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta http-equiv="content-type" content="text/html;charset=utf-8">

<link href="${pageContext.request.contextPath}/css/common.css"
	rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/product.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/desc.css">
</head>

<body>
	<%@ include file="menu.jsp"%>
	<div class="container productList">
		<div class="divOrder">
			<span>订单号：<s:property value="model.oid" /> 
				<s:if test="model.state == 1">
					<font color="red">(等待付款)&nbsp;</font>
				</s:if> <s:if test="model.state == 2">
					<font color="red">(准备发货)&nbsp;</font>
				</s:if> <s:if test="model.state == 3">
					<font color="red">(等待确认)&nbsp;</font>
				</s:if> <s:if test="model.state == 4">
					<font color="red">(交易成功)&nbsp;</font>
				</s:if> <s:if test="model.state == 5">
					<font color="red">(已取消)&nbsp;</font>
				</s:if> 下单时间：<s:date name="model.ordertime" />
			</span>
		</div>
		<div class="divContent">
			<div class="div2">
				<dl>
					<dt>收货人信息</dt>
					<dd>
						姓名：
						<s:property value="model.name" />
					</dd>
					<dd>
						电话：
						<s:property value="model.phone" />
					</dd>
					<dd>
						地址：
						<s:property value="model.addr" />
					</dd>
				</dl>
			</div>
			<div class="div2">
				<dl>
					<dt>商品清单</dt>
					<dd>
						<table cellpadding="0" cellspacing="0">
							<tr>
								<th class="tt">商品名称</th>
								<th class="tt" align="left">单价</th>
								<th class="tt" align="left">数量</th>
								<th class="tt" align="left">小计</th>
							</tr>
							<s:iterator value="model.setOrderItem" var="orderItem">
								<tr style="padding-top: 20px; padding-bottom: 20px;">
									<td class="td" width="400px">
										<div class="bookname">
											<img align="middle" width="70"
												src="<s:property value="#orderItem.product.image"/>" /> <a
												title="<s:property value="#orderItem.product.pname"/>"
												href="${pageContext.request.contextPath }/product_findByPid.action?pid=<s:property value="#orderItem.product.pid"/>"><s:property
													value="#orderItem.product.pname" /></a>
										</div>
									</td>
									<td class="td"><span>&yen;<s:property
												value="#orderItem.product.shop_price" /></span></td>
									<td class="td"><span><s:property
												value="#orderItem.count" /></span></td>
									<td class="td"><span>&yen;<s:property
												value="#orderItem.subtotal" /></span></td>
								</tr>
							</s:iterator>
						</table>
					</dd>
				</dl>
			</div>
			<div style="margin: 10px 10px 10px 550px;">
				<span style="font-weight: 900; font-size: 15px;">合计金额：</span> <span
					class="price_t">&yen;<s:property value="model.total" /></span><br />
				<s:if test="model.state == 1">
					<a
						href="${pageContext.request.contextPath }/order_payOrder?oid=<s:property value="model.oid" />"
						class="pay"></a>
					<br />
				</s:if>
				<s:if test="model.state == 1">
					<a id="cancel"
						href="${pageContext.request.contextPath }/order_cancelOrder?oid=<s:property value="model.oid" />">取消订单</a>
					<br />
				</s:if>
				<s:if test="model.state == 3">
					<a id="confirm"
						href="${pageContext.request.contextPath }/order_confirmOrder?oid=<s:property value="model.oid" />">确认收货</a>
					<br />
				</s:if>
			</div>
		</div>
	</div>


	<div class="container footer">
		<div class="span24">
			<div class="footerAd">
				<img src="image\r___________renleipic_01/footer.jpg" alt="我们的优势"
					title="我们的优势" height="52" width="950" />
			</div>
		</div>
		<div class="span24">
			<ul class="bottomNav">
				<li><a href="#">关于我们</a> |</li>
				<li><a href="#">联系我们</a> |</li>
				<li><a href="#">诚聘英才</a> |</li>
				<li><a href="#">法律声明</a> |</li>
				<li><a>友情链接</a> |</li>
				<li><a target="_blank">支付方式</a> |</li>
				<li><a target="_blank">配送方式</a> |</li>
				<li><a>SHOP++官网</a> |</li>
				<li><a>SHOP++论坛</a></li>
			</ul>
		</div>
		<div class="span24">
			<div class="copyright">Copyright © 2005-2015 网上商城 版权所有</div>
		</div>
	</div>
</body>
</html>

