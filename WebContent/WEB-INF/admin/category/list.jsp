<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/Style1.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function addCategory() {
		window.location.href = "${pageContext.request.contextPath}/adminCategory_addPage.action";
	}
</script>
</HEAD>
<body>
	<br>
	<table cellSpacing="1" cellPadding="0" width="100%" align="center"
		bgColor="#f5fafe" border="0">
		<TBODY>
			<tr>
				<td class="ta_01" align="center" bgColor="#afd1f3"><strong>分类列表</strong>
				</TD>
			</tr>
			<tr>
				<td class="ta_01" align="center">
					<button type="button" id="add" name="add" class="button_add"
						onclick="addCategory()">添加一级分类</button>

				</td>
			</tr>
			<tr class="trTitle">
			</tr>
			<tr>
				<td class="ta_01" align="center" bgColor="#f5fafe">
					<table cellspacing="0" cellpadding="1" rules="all"
						bordercolor="gray" border="1" id="DataGrid1"
						style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; WIDTH: 100%; WORD-BREAK: break-all; BORDER-BOTTOM: gray 1px solid; BORDER-COLLAPSE: collapse; BACKGROUND-COLOR: #f5fafe; WORD-WRAP: break-word">
						<tr
							style="FONT-WEIGHT: bold; FONT-SIZE: 12pt; HEIGHT: 25px; BACKGROUND-COLOR: white">
							<td align="center" width="17%">一级分类名称</td>
							<td width="15%" align="center">操作</td>
						</tr>
						<s:iterator var="c" value="categoryList">
							<tr bgcolor="#afd1f3" >

								<td style="CURSOR: hand; HEIGHT: 22px" align="center"
									width="17%"><s:property value="#c.cname" /></td>
								<td align="center" style="HEIGHT: 22px">
									<a href="${pageContext.request.contextPath }/adminCategorySecond_addPage.action?category.cid=<s:property value="#c.cid"/>">添加二级分类</a>&nbsp;&nbsp;
									<a
										href="${pageContext.request.contextPath }/adminCategory_edit.action?cid=<s:property value="#c.cid"/>">&nbsp;&nbsp;修改
											<img
											src="${pageContext.request.contextPath}/images/i_edit.gif"
											border="0" style="CURSOR: hand">
									</a>
									<a onclick="return confirm('您是否真要删除该一级分类？')"
										href="${ pageContext.request.contextPath }/adminCategory_delete.action?cid=<s:property value="#c.cid"/>">&nbsp;&nbsp;删除
											<img src="${pageContext.request.contextPath}/images/i_del.gif"
											width="16" height="16" border="0" style="CURSOR: hand">
									</a>
								</td>
							</tr>
							
							<s:iterator value="#c.setCategorySecond" var="categorySecond">
								<tr onmouseover="this.style.backgroundColor = 'white'"
								onmouseout="this.style.backgroundColor = '#F5FAFE';">

								<td style="CURSOR: hand; HEIGHT: 22px" align="center"
									width="17%"><s:property value="#categorySecond.csname" /></td>
								<td align="center" style="HEIGHT: 22px">
									<a
										href="${ pageContext.request.contextPath }/adminCategorySecond_edit.action?csid=<s:property value="#categorySecond.csid"/>">&nbsp;&nbsp;修改
											<img
											src="${pageContext.request.contextPath}/images/i_edit.gif"
											border="0" style="CURSOR: hand">
									</a>
									<a onclick="return confirm('您是否真要删除该二级分类？')"
										href="${ pageContext.request.contextPath }/adminCategorySecond_delete.action?csid=<s:property value="#categorySecond.csid"/>">&nbsp;&nbsp;删除
											<img src="${pageContext.request.contextPath}/images/i_del.gif"
											width="16" height="16" border="0" style="CURSOR: hand">
									</a>
								</td>
							</tr>
							
							</s:iterator >
						</s:iterator>
					</table>
				</td>
			</tr>
		</TBODY>
	</table>
</body>
</HTML>

