<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>网上商城管理中心</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
body {
	color: white;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	$(function() {
		$("#loginForm").submit(function() {
			if (!validateUsername()) {
				return false;
			}
			if (!validatePassword()) {
				return false;
			}
			return true;
		});
	});
	function validateUsername() {
		var id = "username";
		var value = $("#" + id).val();
		if (!value) {
			alert("用户名不能为空！");
			return false;
		}
		if (!/\w{3,10}$/.test(value)) {
			alert("用户名格式错误！");
			return false;
		}
		return true;
	}

	function validatePassword() {
		var id = "password";
		var value = $("#" + id).val();

		if (!value) {
			alert("密码不能为空！");
			return false;
		}
		return true;
	}
</script>
</head>
<body style="background: #278296">
	<form method="post" id="loginForm" target="_parent"
		action="${pageContext.request.contextPath }/admin_login.action">
		<table cellspacing="0" cellpadding="0" style="margin-top: 100px"
			align="center">
			<tr>
				<td style="padding-left: 50px">
					<table>
						<tr>
							<td></td>
							<td><font color="red">${msg }</font></td>
						</tr>
						<tr>
							<td>管理员姓名：</td>
							<td><input type="text" name="username" id="username" /></td>
						</tr>
						<tr>
							<td>管理员密码：</td>
							<td><input type="password" name="password" id="password" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td><input type="submit" value="进入管理中心" class="button" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<input type="hidden" name="act" value="signin" />
	</form>
</body>
</html>