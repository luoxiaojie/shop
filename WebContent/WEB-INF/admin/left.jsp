<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>菜单</title>
<link href="${pageContext.request.contextPath}/css/left.css"
	rel="stylesheet" type="text/css" />
<link rel="StyleSheet"
	href="${pageContext.request.contextPath}/css/dtree.css" type="text/css" />
</head>
<body>
	<table width="100" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="12"></td>
		</tr>
	</table>
	<table width="100%" border="0">
		<tr>
			<td>
				<div class="dtree">

					<a href="javascript: d.openAll();">展开所有</a> | <a
						href="javascript: d.closeAll();">关闭所有</a>

					<script type="text/javascript"
						src="${pageContext.request.contextPath}/js/dtree.js"></script>
					<script type="text/javascript">
						d = new dTree('d');
						d.add('01', -1, '系统菜单树');
						d.add('0101', '01', '用户管理', '', '', 'mainFrame');
						d
								.add(
										'010101',
										'0101',
										'客户信息管理',
										'${pageContext.request.contextPath}/adminUser_findAllUser.action',
										'', 'mainFrame');
						d
						.add(
								'010102',
								'0101',
								'管理员信息管理',
								'${pageContext.request.contextPath}/admin_findAllAdmin.action',
								'', 'mainFrame');
						d.add('0102', '01', '分类管理', '', '', 'mainFrame');
						d
								.add(
										'010201',
										'0102',
										'分类管理',
										'${pageContext.request.contextPath}/adminCategory_findAll.action',
										'', 'mainFrame');
						d.add('0103', '01', '商品管理');
						d
								.add(
										'010301',
										'0103',
										'所有商品管理',
										'${pageContext.request.contextPath}/adminProduct_findAll.action',
										'', 'mainFrame');
						d
						.add(
								'010302',
								'0103',
								'商品入库管理',
								'${pageContext.request.contextPath}/adminStock_findAllInStock.action',
								'', 'mainFrame');
						d
						.add(
								'010303',
								'0103',
								'商品出库管理',
								'${pageContext.request.contextPath}/adminStock_findAllOutStock.action',
								'', 'mainFrame');
						d.add('0104', '01', '订单管理');
						d
								.add(
										'010401',
										'0104',
										'订单管理',
										'${pageContext.request.contextPath}/adminOrder_findAll.action',
										'', 'mainFrame');
						d.add('0105', '01', '供应商管理');
						d
								.add(
										'010501',
										'0105',
										'供应商管理',
										'${pageContext.request.contextPath}/adminSupplier_findAllSupplier.action',
										'', 'mainFrame');
						document.write(d);
					</script>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
