<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/css/Style1.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/pager.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	function addProduct() {
		window.location.href = "${pageContext.request.contextPath}/adminProduct_addPage.action";
	}
</script>
</HEAD>
<body>
	<br>
	<form id="Form1" name="Form1"
		action="${pageContext.request.contextPath}/user/list.jsp"
		method="post">
		<table cellSpacing="1" cellPadding="0" width="100%" align="center"
			bgColor="#f5fafe" border="0">
			<TBODY>
				<tr>
					<td class="ta_01" align="center" bgColor="#afd1f3"><strong>商品列表</strong>
					</TD>
				</tr>
				<tr>
					<td class="ta_01" align="right">
						<button type="button" id="add" name="add" value="添加"
							class="button_add" onclick="addProduct()">
							&#28155;&#21152;</button>

					</td>
				</tr>
				<tr>
					<td class="ta_01" align="center" bgColor="#f5fafe">
						<table cellspacing="0" cellpadding="1" rules="all"
							bordercolor="gray" border="1" id="DataGrid1"
							style="BORDER-RIGHT: gray 1px solid; BORDER-TOP: gray 1px solid; BORDER-LEFT: gray 1px solid; WIDTH: 100%; WORD-BREAK: break-all; BORDER-BOTTOM: gray 1px solid; BORDER-COLLAPSE: collapse; BACKGROUND-COLOR: #f5fafe; WORD-WRAP: break-word">
							<tr
								style="FONT-WEIGHT: bold; FONT-SIZE: 12pt; HEIGHT: 25px; BACKGROUND-COLOR: #afd1f3">

								<td align="center" width="7%">商品编号</td>
								<td align="center" width="7%">商品图片</td>
								<td align="center" width="20%">商品名称</td>
								<td align="center" width="5%">商品价格</td>
								<td align="center" width="5%">库存数量</td>
								<td align="center" width="25%">商品描述</td>
								<td align="center" width="7%">所属分类</td>
								<td align="center" width="3%">是否热门</td>
								<td align="center" width="10%">上市时间</td>
								<td align="center" width="5%">供应商</td>
								<td width="3%" align="center">编辑</td>
								<td width="10%" align="center">删除</td>
							</tr>
							<s:iterator var="p" value="pageBean.list">
								<tr onmouseover="this.style.backgroundColor = 'white'"
									onmouseout="this.style.backgroundColor = '#F5FAFE';">
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="7%"><s:property value="#p.pid" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="7%"><img width="40" height="45"
										src="${pageContext.request.contextPath }/<s:property value="#p.image"/>">
									</td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="20%"><s:property value="#p.pname" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="5%"><s:property value="#p.shop_price" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="5%"><s:property value="#p.quantity" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="25%"><s:property value="#p.pdesc" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="7%"><s:property value="#p.categorySecond.csname" /></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="3%"><s:if test="#p.is_hot==1">
													是
												</s:if> <s:else>
													否
												</s:else></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="10%"><s:date name="#p.pdate"/></td>
									<td style="CURSOR: hand; HEIGHT: 22px" align="center"
										width="5%"><s:property value="#p.supplier.sname" /></td>
									<td align="center" style="HEIGHT: 22px"><a
										href="${ pageContext.request.contextPath }/adminProduct_edit.action?pid=<s:property value="#p.pid"/>">
											<img
											src="${pageContext.request.contextPath}/images/i_edit.gif"
											border="0" style="CURSOR: hand">
									</a></td>

									<td align="center" style="HEIGHT: 22px"><a
										onclick="return confirm('您是否真要删除该商品？')"
										href="${ pageContext.request.contextPath }/adminProduct_delete.action?pid=<s:property value="#p.pid"/>">
											<img
											src="${pageContext.request.contextPath}/images/i_del.gif"
											width="16" height="16" border="0" style="CURSOR: hand">
									</a></td>
								</tr>
							</s:iterator>
						</table>
					</td>
				</tr>
				<tr align="center">
					<td colspan="7"><%@include file="../../jsp/pager.jsp"%>
					</td>
				</tr>
			</TBODY>
		</table>
	</form>
</body>
</HTML>

