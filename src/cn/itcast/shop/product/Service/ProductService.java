package cn.itcast.shop.product.Service;

import java.util.List;

import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.product.domain.Product;
import cn.itcast.shop.utils.pagebean.PageBean;

public interface ProductService {

	List<Product> findHot();

	List<Product> findNew();

	Product findByPid(Integer pid);

	/**
	 * 
	 * @Title: findByCid
	 * @Description:
	 * @param cid
	 * @param page
	 * @return PageBean<Product>
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:45:05
	 */
	PageBean<Product> findByCid(Integer cid, int page);

	PageBean<Product> findByCsid(Integer csid, int page);

	PageBean<Product> findAll(int page);

	List<CategorySecond> findAllCategorySecond();

	/**
	 * 
	 * @Title: save
	 * @Description:
	 * @param product void
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:45:19
	 */
	void save(Product product);

	/**
	 * 
	 * @Title: update
	 * @Description:
	 * @param product void
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:45:33
	 */
	void update(Product product);

	void delete(Product product);
	
}
