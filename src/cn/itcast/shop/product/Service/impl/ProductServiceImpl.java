package cn.itcast.shop.product.Service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.product.Service.ProductService;
import cn.itcast.shop.product.dao.ProductDao;
import cn.itcast.shop.product.domain.Product;
import cn.itcast.shop.utils.pagebean.PageBean;

@Service("productService")
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDao productDao;

	@Override
	public List<Product> findHot() {
		return productDao.findHot();
	}

	@Override
	public List<Product> findNew() {
		return productDao.findNew();
	}

	@Override
	public Product findByPid(Integer pid) {
		return productDao.findByPid(pid);
	}

	/**
	 * (non-Javadoc)   
	* @see cn.itcast.shop.product.Service.ProductService#findByCid(java.lang.Integer, int)
	 */
	@Override
	public PageBean<Product> findByCid(Integer cid, int page) {
		PageBean<Product> pageBean = new PageBean<>();
		int limit = 12;
		pageBean.setLimit(limit);
		if(page==0)page=1;
		pageBean.setPage(page);
		
		int totalCount =productDao.findCountByCid(cid);
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Product> list = productDao.findByCid(cid,begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	/**
	 * (non-Javadoc)   
	* @see cn.itcast.shop.product.Service.ProductService#findByCsid(java.lang.Integer, int)
	 */
	@Override
	public PageBean<Product> findByCsid(Integer csid, int page) {
		PageBean<Product> pageBean = new PageBean<>();
		int limit = 12;
		pageBean.setLimit(limit);
		if(page==0)page=1;
		pageBean.setPage(page);
		
		int totalCount =productDao.findCountByCsid(csid);
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Product> list = productDao.findByCsid(csid,begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public PageBean<Product> findAll(int page) {
		PageBean<Product> pageBean = new PageBean<>();
		int limit = 20;
		pageBean.setLimit(limit);
		if(page==0)page=1;
		pageBean.setPage(page);
		
		int totalCount =productDao.findAllCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Product> list = productDao.findAll(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public List<CategorySecond> findAllCategorySecond() {
		return productDao.findAllCategorySecond();
	}

	@Override
	public void save(Product product) {
		productDao.save(product);
	}

	@Override
	public void update(Product product) {
		productDao.update(product);
	}

	@Override
	public void delete(Product product) {
		productDao.delete(product);
	}
}
