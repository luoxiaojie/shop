package cn.itcast.shop.product.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.product.Service.ProductService;
import cn.itcast.shop.product.domain.Product;
import cn.itcast.shop.utils.pagebean.PageBean;
/**
 * 商品模块Action
 */
public class ProductAction extends ActionSupport implements ModelDriven<Product>{

	private static final long serialVersionUID = -1060309096032393154L;

	/**
	 * 商品业务层
	 */
	@Autowired
	private ProductService productService;
	
	/**
	 * 模型驱动
	 */
	private Product product = new Product();
	
	/**
	 * 一级分类ID
	 */
	private Integer cid;
	
	/**
	 * 二级分类ID
	 */
	private Integer csid;
	
	/**
	 * 页码
	 */
	private int page;
	
	/**
	 * 分页对象
	 */
	private PageBean<Product> pageBean;
	
	public void setCsid(Integer csid) {
		this.csid = csid;
	}
	public PageBean<Product> getPageBean() {
		return pageBean;
	}
	
	public void setPage(int page) {
		this.page = page;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public Integer getCid() {
		return cid;
	}
	@Override
	public Product getModel() {
		return product;
	}
	
	/**
	 * 获取请求路径
	 */
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	/**
	 * 按商品编号查询
	 * @return
	 */
	public String findByPid(){
		product = productService.findByPid(product.getPid());
		return "productPage";
	}
	/**
	 * 按一级分类查询
	 * @return
	 */
	public String findByCid(){
		pageBean= productService.findByCid(cid,page);
		pageBean.setUrl(getUrl());
		return "productListPage";
	}
	/**
	 * 按二级分类查询
	 * @return
	 */
	public String findByCsid(){
		pageBean= productService.findByCsid(csid,page);
		pageBean.setUrl(getUrl());
		return "productListPage";
	}
		
}
