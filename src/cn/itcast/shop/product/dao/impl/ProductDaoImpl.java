package cn.itcast.shop.product.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.product.dao.ProductDao;
import cn.itcast.shop.product.domain.Product;

@Repository("productDao")
@Transactional
public class ProductDaoImpl implements ProductDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}
	/**
	 * 查询热门商品
	 */
	@Override
	public List<Product> findHot() {
		String hql = "from Product where is_hot=1 order by pdate desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(10);
		@SuppressWarnings("unchecked")
		List<Product> list = query.list();
		return list;
	}
	/**
	 * 查询最新的商品
	 */
	@Override
	public List<Product> findNew() {
		String hql = "from Product order by pdate desc";
		Query query = getSession().createQuery(hql);
		query.setFirstResult(0);
		query.setMaxResults(10);
		@SuppressWarnings("unchecked")
		List<Product> list = query.list();
		return list;
	}
	/**
	 * 查询一件商品
	 */
	@Override
	public Product findByPid(Integer pid) {
		Product product = (Product) getSession().get(Product.class, pid);
		return product;
	}
	/**
	 * 查询一个一级分类下所有商品的数量
	 */
	@Override
	public int findCountByCid(Integer cid) {
		String hql = "select count(*) from Product p where p.categorySecond.category.cid=?";
		Long l = (Long) getSession().createQuery(hql).setParameter(0, cid).uniqueResult();
		return l.intValue();
	}
	/**
	 * 查询一级分类下所有商品
	 */
	@Override
	public List<Product> findByCid(Integer cid, int begin, int limit) {
		String hql = "select p from Product p inner join fetch p.categorySecond cs inner join fetch cs.category c where c.cid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, cid);
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Product> list = query.list();
		return list;
	}
	@Override
	public int findCountByCsid(Integer csid) {
		String hql = "select count(*) from Product p where p.categorySecond.csid=?";
		Long l = (Long) getSession().createQuery(hql).setParameter(0, csid).uniqueResult();
		return l.intValue();
	}
	@Override
	public List<Product> findByCsid(Integer csid, int begin, int limit) {
		String hql = "select p from Product p inner join fetch p.categorySecond cs where cs.csid=?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, csid);
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Product> list = query.list();
		return list;
	}
	@Override
	public List<Product> findAll(int begin , int limit ) {
		Query query = getSession().createQuery("from Product order by pid desc ");
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Product> list = query.list();
		return list;
	}
	@Override
	public int findAllCount() {
		String hql = "select count(*) from Product";
		Long l = (Long) getSession().createQuery(hql).uniqueResult();
		return l.intValue();
	}
	@Override
	public List<CategorySecond> findAllCategorySecond() {
		String hql = "from CategorySecond";
		@SuppressWarnings("unchecked")
		List<CategorySecond> list = getSession().createQuery(hql).list();
		return list;
	}
	@Override
	public void save(Product product) {
		getSession().save(product);
	}
	@Override
	public void update(Product product) {
		getSession().update(product);
	}
	@Override
	public void delete(Product product) {
		getSession().delete(product);
	}

}
