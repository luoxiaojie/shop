package cn.itcast.shop.product.dao;

import java.util.List;

import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.product.domain.Product;

public interface ProductDao {

	List<Product> findHot();

	List<Product> findNew();

	Product findByPid(Integer pid);

	int findCountByCid(Integer cid);

	List<Product> findByCid(Integer cid, int begin, int limit);

	int findCountByCsid(Integer csid);

	List<Product> findByCsid(Integer csid, int begin, int limit);

	List<Product> findAll(int begin,int limit);

	int findAllCount();

	List<CategorySecond> findAllCategorySecond();

	void save(Product product);

	void update(Product product);

	void delete(Product product);


}
