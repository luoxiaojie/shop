package cn.itcast.shop.category.dao;

import java.util.List;

import cn.itcast.shop.category.domain.Category;

public interface CategoryDao {

	List<Category> findAll();

	Category findByCid(Integer cid);

	void update(Category category);

	void delete(Category category);

	void save(Category category);

}
