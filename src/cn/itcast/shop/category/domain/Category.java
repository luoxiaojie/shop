package cn.itcast.shop.category.domain;

import java.util.HashSet;
import java.util.Set;

import cn.itcast.shop.categorysecond.domain.CategorySecond;

/**
 * 一级分类实体类
 */
public class Category {
	/**
	 * 一级分类ID
	 */
	private Integer cid;
	/**
	 * 一级分类名字
	 */
	private String cname;
	/**
	 * 一级分类下所有的二级分类
	 */
	private Set<CategorySecond> setCategorySecond = new HashSet<CategorySecond>();
	/**
	 * 获取 cid
	 * @return the cid
	 */
	public Integer getCid() {
		return cid;
	}
	/**
	 * 设置 cid
	 * @param cid the cid to set
	 */
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	/**
	 * 获取 cname
	 * @return the cname
	 */
	public String getCname() {
		return cname;
	}
	/**
	 * 设置 cname
	 * @param cname the cname to set
	 */
	public void setCname(String cname) {
		this.cname = cname;
	}
	/**
	 * 获取 setCategorySecond
	 * @return the setCategorySecond
	 */
	public Set<CategorySecond> getSetCategorySecond() {
		return setCategorySecond;
	}
	/**
	 * 设置 setCategorySecond
	 * @param setCategorySecond the setCategorySecond to set
	 */
	public void setSetCategorySecond(Set<CategorySecond> setCategorySecond) {
		this.setCategorySecond = setCategorySecond;
	}
}
