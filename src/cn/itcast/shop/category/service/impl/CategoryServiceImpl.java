package cn.itcast.shop.category.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.category.dao.CategoryDao;
import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.category.service.CategoryService;

/**
 * 一级分类业务层
 */
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{
	/**
	 * 一级分类的数据访问对象
	 */
	@Autowired
	private CategoryDao categoryDao;

	/**
	 * 查询所有的一级分类
	 */
	@Override
	public List<Category> findAll() {
		return categoryDao.findAll();
	}

	/**
	 * 通过指定的一级分类ID查询
	 */
	@Override
	public Category findByCid(Integer cid) {
		return categoryDao.findByCid(cid);
	}

	/**
	 * 修改一级分类
	 */
	@Override
	public void update(Category category) {
		categoryDao.update(category);
	}

	/**
	 * 删除一级分类
	 */
	@Override
	public void delete(Category category) {
		categoryDao.delete(category);
	}

	/**
	 * 新增一级分类
	 */
	@Override
	public void save(Category category) {
		categoryDao.save(category);
	}
}
