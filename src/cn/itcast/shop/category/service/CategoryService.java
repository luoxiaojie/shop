package cn.itcast.shop.category.service;

import java.util.List;

import cn.itcast.shop.category.domain.Category;

public interface CategoryService {

	List<Category> findAll();

	Category findByCid(Integer cid);

	void update(Category category);

	void delete(Category category);

	void save(Category category);

}
