package cn.itcast.shop.user.dao;

import java.util.List;

import cn.itcast.shop.user.domain.User;

public interface UserDao {
	public User findByUsername(String username);

	public User findByEmail(String email);

	public void regist(User user);

	public User findByActivationCode(String activationCode);

	public void updateStatus(Integer uid, int i);

	public User login(String username, String password);

	public List<User> findAllUser(int begin, int limit);

	public int getUserCount();

	public User findUserByUid(Integer uid);

	public void update(User user);

	public void delete(Integer uid);
}
