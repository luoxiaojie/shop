package cn.itcast.shop.user.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.user.dao.UserDao;
import cn.itcast.shop.user.domain.User;

@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao {

	/**
	 * 注入sessionFactory对象
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * 获得当前线程的session对象
	 * @return
	 */
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * 通过用户名查询用户
	 */
	@Override
	public User findByUsername(String username) {
		String hql = "from User where username = ?";
		@SuppressWarnings("unchecked")
		List<User> list = getSession().createQuery(hql).setParameter(0, username).list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 通过邮箱名查询用户
	 */
	@Override
	public User findByEmail(String email) {
		String hql = "from User where email = ?";
		@SuppressWarnings("unchecked")
		List<User> list = getSession().createQuery(hql).setParameter(0, email).list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 保存新用户
	 */
	@Override
	public void regist(User user) {
		getSession().save(user);

	}

	/**
	 * 通过激活码查询用户
	 */
	@Override
	public User findByActivationCode(String activationCode) {
		String hql = "from User where code = ?";
		@SuppressWarnings("unchecked")
		List<User> list = getSession().createQuery(hql).setParameter(0, activationCode).list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 更新用户激活状态
	 */
	@Override
	public void updateStatus(Integer uid, int i) {
		User user = (User) getSession().get(User.class, uid);
		user.setState(i);
	}

	/**
	 * 根据账户密码查询用户
	 */
	@Override
	public User login(String username, String password) {
		String hql = "from User where username=? and password=?";
		@SuppressWarnings("unchecked")
		List<User> list = getSession().createQuery(hql)
					.setParameter(0, username).setParameter(1, password).list();
		if (list != null && list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 查询所有用户
	 */
	@Override
	public List<User> findAllUser(int begin, int limit) {
		Query query = getSession().createQuery("from User order by uid desc ");
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<User> list = query.list();
		return list;
	}

	/**
	 * 查询用户数量
	 */
	@Override
	public int getUserCount() {
		String hql = "select count(*) from User ";
		int count = ((Long)getSession().createQuery(hql).uniqueResult()).intValue();
		return count;
	}

	/**
	 * 通过用户id查询
	 */
	@Override
	public User findUserByUid(Integer uid) {
		User user = (User) getSession().get(User.class, uid);
		return user;
	}

	/**
	 * 更新用户信息
	 */
	@Override
	public void update(User user) {
		getSession().update(user);
	}

	/**
	 * 删除用户
	 */
	@Override
	public void delete(Integer uid) {
		User user = (User) getSession().get(User.class,uid);
		getSession().delete(user);
		
	}

	
}
