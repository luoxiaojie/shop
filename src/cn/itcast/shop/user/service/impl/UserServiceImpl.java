package cn.itcast.shop.user.service.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.user.dao.UserDao;
import cn.itcast.shop.user.domain.User;
import cn.itcast.shop.user.exception.UserException;
import cn.itcast.shop.user.service.UserService;
import cn.itcast.shop.utils.pagebean.PageBean;
import cn.mytools.commons.CommonUtils;
import cn.mytools.mail.Mail;
import cn.mytools.mail.MailUtils;
/**
 * 用户的业务层
 */
@Service("userService")
public class UserServiceImpl implements UserService {
	
	/**
	 * 用户数据访问层
	 */
	@Autowired
	private UserDao userDao;
	
	/**
	 * 按用户名查询
	 */
	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

	/**
	 * 按邮箱查询
	 */
	@Override
	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}

	/**
	 * 注册方法
	 */
	@Override
	public void regist(User user) {
		user.setState(0);//设置激活状态为未激活
		user.setCode(CommonUtils.uuid()+CommonUtils.uuid());//生成两个32位随机字符串，相当于64位，永远不会重复。
		userDao.regist(user);
		
		Properties prop = new Properties();

		try {
			// 加载邮箱模板配置文件
			prop.load(this.getClass().getClassLoader().getResourceAsStream("email_template.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		//设置邮箱服务器host
		String host = prop.getProperty("host");
		//设置邮箱账户
		String username = prop.getProperty("username");
		//设置邮箱密码
		String password = prop.getProperty("password");
		//创建邮件session对象
		Session session = MailUtils.createSession(host, username, password);

		String from = prop.getProperty("from");
		String to = user.getEmail();
		String subject = prop.getProperty("subject");
		// MessageFormat.format 会把第一个参数中的{0}占位符，使用第二个参数来替换。
		// 例如 MessageFormat.format(你好{0}，你{1}，张三，去屎吧); 返回你好张三，你去屎把
		String content = MessageFormat.format(prop.getProperty("content"),user.getName(), user.getCode());
		Mail mail = new Mail(from, to, subject, content);
		try {
			MailUtils.send(session, mail);//发送邮件
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
	}

	/**
	 * 用户激活
	 */
	@Override
	public void activation(String activationCode) throws UserException {
		User user = userDao.findByActivationCode(activationCode);
		if (user == null){
			throw new UserException("无效的激活码！");
		}else if (user.getState() == 1){
			throw new UserException("已经激活过了，不要二次激活！");
		}else{
			userDao.updateStatus(user.getUid(), 1);
		}
	}

	/**
	 * 登录方法
	 */
	@Override
	public User login(User user) {
		return userDao.login(user.getUsername(),user.getPassword());
	}

	/**
	 * 查询所有用户
	 */
	@Override
	public PageBean<User> findAllUser(int page) {
		//创建分页对象
		PageBean<User> pageBean = new PageBean<>();
		if(page==0)page=1;
		int limit = 10;
		pageBean.setLimit(limit);
		pageBean.setPage(page);
		
		int totalCount = userDao.getUserCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<User> list = userDao.findAllUser(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	/**
	 * 通过ID查询用户
	 */
	@Override
	public User findUserByUid(Integer uid) {
		return userDao.findUserByUid(uid);
	}

	/**
	 * 更新用户状态
	 */
	@Override
	public void update(User user) {
		userDao.update(user);
	}

	/**
	 * 删除用户
	 */
	@Override
	public void delete(Integer uid) {
		userDao.delete(uid);
	}
}
