package cn.itcast.shop.user.service;

import cn.itcast.shop.user.domain.User;
import cn.itcast.shop.user.exception.UserException;
import cn.itcast.shop.utils.pagebean.PageBean;

public interface UserService {
	public User findByUsername(String username);

	public User findByEmail(String email);

	public void regist(User user);

	public void activation(String activationCode) throws UserException;

	public User login(User user);

	public PageBean<User> findAllUser(int page);

	public User findUserByUid(Integer uid);

	public void update(User user);

	public void delete(Integer uid);
}
