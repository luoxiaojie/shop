package cn.itcast.shop.categorysecond.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.categorysecond.domain.CategorySecond;

/**
 * 二级分类的数据操作层
 ******************************************
 * @author HuangFeng  [2017年4月20日 上午11:32:30]
 * @version 1.0
 ******************************************
 */
@Repository
@Transactional
public class CategorySecondDao {
	
	/**
	 * 注入session工厂对象
	 */
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * 获得session
	 */
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	/**
	 * 查询二级分类
	 */
	public CategorySecond findByCsid(Integer csid) {
		CategorySecond categorySecond = (CategorySecond) getSession().get(CategorySecond.class, csid);
		return categorySecond;
	}

	/**
	 * 修改二级分类
	 */
	public void update(CategorySecond categorySecond) {
		getSession().update(categorySecond);
	}

	/**
	 * 删除二级分类
	 */
	public void delete(CategorySecond categorySecond) {
		getSession().delete(categorySecond);
	}

	/**
	 * 查询所有二级分类
	 */
	public List<Category> findAll() {
		@SuppressWarnings("unchecked")
		List<Category> list = getSession().createQuery("from Category").list();
		return list;
	}

	/**
	 * 添加二级分类
	 */
	public void save(CategorySecond categorySecond) {
		getSession().save(categorySecond);
	}
}
