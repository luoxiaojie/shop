package cn.itcast.shop.categorysecond.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.categorysecond.dao.CategorySecondDao;
import cn.itcast.shop.categorysecond.domain.CategorySecond;

/**
 * 二级分类业务层
 */
@Service
public class CategorySecondService {
	
	/**
	 * 注入二级分类数据访问对象
	 */
	@Autowired
	private CategorySecondDao categorySecondDao;

	/**
	 * 查询二级分类
	 */
	public CategorySecond findByCsid(Integer csid) {
		return categorySecondDao.findByCsid(csid);
	}

	/**
	 * 修改二级分类
	 */
	public void update(CategorySecond categorySecond) {
		categorySecondDao.update(categorySecond);
	}

	/**
	 * 删除二级分类
	 */
	public void delete(CategorySecond categorySecond) {
		categorySecondDao.delete(categorySecond);
	}

	/**
	 * 查询所有二级分类
	 */
	public List<Category> findAll() {
		return categorySecondDao.findAll();
	}

	/**
	 * 添加二级分类
	 */
	public void save(CategorySecond categorySecond) {
		categorySecondDao.save(categorySecond);
	}
}
