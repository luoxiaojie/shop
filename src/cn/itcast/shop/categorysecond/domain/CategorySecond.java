package cn.itcast.shop.categorysecond.domain;

import java.util.HashSet;
import java.util.Set;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.product.domain.Product;

public class CategorySecond {
	
	/**
	 * 二级分类ID
	 */
	private Integer csid;
	
	/**
	 * 二级分类名字
	 */
	private String csname;
	
	/**
	 * 所属的一级分类
	 */
	private Category category;
	
	/**
	 * 二级分类下所有的商品
	 */
	private Set<Product> setProduct = new HashSet<>();
	
	public Set<Product> getSetProduct() {
		return setProduct;
	}
	public void setSetProduct(Set<Product> setProduct) {
		this.setProduct = setProduct;
	}
	public Integer getCsid() {
		return csid;
	}
	public void setCsid(Integer csid) {
		this.csid = csid;
	}
	public String getCsname() {
		return csname;
	}
	public void setCsname(String csname) {
		this.csname = csname;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
}
