package cn.itcast.shop.cart.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.cart.dao.CartDao;
import cn.itcast.shop.cart.domain.CartItem;
import cn.itcast.shop.cart.service.CartService;

/**
 * 购物车模块的业务层
 */
@Service
public class CartServiceImpl implements CartService {
	
	/**
	 * 注入数据操作对象
	 */
	@Autowired
	private CartDao cartDao;

	/**
	 * 添加购物条目
	 */
	@Override
	public void addCartItem(CartItem cartItem) {
		cartDao.addCartItem(cartItem);
	}

	/**
	 * 查询购物条目
	 */
	@Override
	public List<CartItem> findByUid(Integer uid) {
		return cartDao.findByUid(uid);
	}

	/**
	 * 删除购物条目
	 */
	@Override
	public void removeCartItemByPid(Integer uid, Integer pid) {
		cartDao.removeCartItemByPid(uid,pid);

	}

	/**
	 * 清除购物车
	 */
	@Override
	public void clearCart(Integer uid) {
		cartDao.clearCart(uid);
		
	}

}
