package cn.itcast.shop.cart.service;

import java.util.List;

import cn.itcast.shop.cart.domain.CartItem;

public interface CartService {

	void addCartItem(CartItem cartItem);

	List<CartItem> findByUid(Integer uid);

	void removeCartItemByPid(Integer uid, Integer pid);

	void clearCart(Integer uid);

}
