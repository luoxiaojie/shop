package cn.itcast.shop.cart.domain;

import java.math.BigDecimal;

/**
 * 购物车条目实体类
 */
public class CartItem {
	/**
	 * 购物车条目ID
	 */
	private Integer cartItemId;
	/**
	 * 此条目对应商品图片路径
	 */
	private String image;
	/**
	 * 商品名
	 */
	private String pname;
	/**
	 * 商品价格
	 */
	private Double shop_price;
	/**
	 * 数量
	 */
	private Integer quantity;
	/**
	 * 商品ID
	 */
	private Integer pid;
	/**
	 * 对应用户ID
	 */
	private Integer uid;

	public Integer getCartItemId() {
		return cartItemId;
	}

	public void setCartItemId(Integer cartItemId) {
		this.cartItemId = cartItemId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public Double getShop_price() {
		return shop_price;
	}

	public void setShop_price(Double shop_price) {
		this.shop_price = shop_price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public BigDecimal getSubtotal() {
		BigDecimal b1 = new BigDecimal(shop_price + "");
		BigDecimal b2 = new BigDecimal(quantity + "");
		BigDecimal b3 = b1.multiply(b2);
		return b3;
	}

}
