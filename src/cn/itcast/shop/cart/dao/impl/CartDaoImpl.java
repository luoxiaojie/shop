package cn.itcast.shop.cart.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.cart.dao.CartDao;
import cn.itcast.shop.cart.domain.CartItem;
import cn.itcast.shop.product.domain.Product;

@Repository("cartDao")
@Transactional
public class CartDaoImpl implements CartDao {

	/**
	 * 注入Session工厂对象
	 */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * 获得session
	 */
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	/**
	 * 添加购物车条目
	 */
	@Override
	public void addCartItem(CartItem cartItem) {
		//创建hql语句，通过商品ID和用户ID查询某一条购物车条目
		String hql = "from CartItem where pid=? and uid=?";
		@SuppressWarnings("unchecked")
		//给hql中的？参数赋值并查询
		List<CartItem> list = getSession().createQuery(hql).setParameter(0, cartItem.getPid())
				.setParameter(1, cartItem.getUid()).list();
		//如果已经存在此商品的条目
		if (list != null && list.size() > 0) {
			//获得原来此条目的商品数量
			Integer quantity = list.get(0).getQuantity();
			//更新此条目的商品数量
			list.get(0).setQuantity(quantity + cartItem.getQuantity());
			getSession().save(list.get(0));

		} else {
			//通过商品ID去查询出商品信息
			Product product = (Product) getSession().get(Product.class, cartItem.getPid());
			//获得商品的图片，名称，价格等信息放入购物条目中
			cartItem.setImage(product.getImage());
			cartItem.setPname(product.getPname());
			cartItem.setShop_price(product.getShop_price());
			//保存购物车条目
			getSession().save(cartItem);
		}

	}

	/**
	 * 查询某一用户下所有的购物车条目
	 */
	@Override
	public List<CartItem> findByUid(Integer uid) {
		String hql = "from CartItem where uid=?";
		@SuppressWarnings("unchecked")
		List<CartItem> listCartItem = getSession().createQuery(hql).setParameter(0, uid).list();
		return listCartItem;
	}

	/**
	 * 删除某一用户下指定的购物车条目
	 */
	@Override
	public void removeCartItemByPid(Integer uid, Integer pid) {
		String hql = "from CartItem where uid=? and pid=?";
		CartItem cartItem = (CartItem) getSession().createQuery(hql).setParameter(0, uid)
						.setParameter(1, pid).list().get(0);
		getSession().delete(cartItem);
		
	}
	
	/**
	 * 删除某一用户下所有的购物车条目
	 */
	@Override
	public void clearCart(Integer uid) {
		String hql = "from CartItem where uid=?" ;
		@SuppressWarnings("unchecked")
		List<CartItem> list = getSession().createQuery(hql).setParameter(0, uid).list();
		for(CartItem cartItem : list){
			getSession().delete(cartItem);
		}

		
	}
}
