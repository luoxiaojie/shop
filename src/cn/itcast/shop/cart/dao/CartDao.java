package cn.itcast.shop.cart.dao;

import java.util.List;

import cn.itcast.shop.cart.domain.CartItem;

public interface CartDao {

	void addCartItem(CartItem cartItem);

	List<CartItem> findByUid(Integer uid);

	void removeCartItemByPid(Integer uid, Integer pid);

	void clearCart(Integer uid);

}
