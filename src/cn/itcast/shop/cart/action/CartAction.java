package cn.itcast.shop.cart.action;

import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.shop.cart.domain.CartItem;
import cn.itcast.shop.cart.service.CartService;
import cn.itcast.shop.user.domain.User;

/**
 * 购物车模块Action
 *
 */
public class CartAction extends ActionSupport{

	private static final long serialVersionUID = 9141463813690554801L;
	
	/**
	 * 注入购物车业务层对象
	 */
	@Autowired
	private CartService cartService;
	
	/**
	 * 商品ID
	 */
	private Integer pid;
	/**
	 * 商品数量
	 */
	private Integer quantity;
	
	/**
	 * 购物条目
	 */
	private List<CartItem> listCartItem;
	
	/**
	 * 添加购物车条目
	 * @return
	 */
	public String addCartItem(){
		//创建一个购物车条目空对象
		CartItem cartItem = new CartItem();
		cartItem.setPid(pid);//设置商品ID
		cartItem.setQuantity(quantity);//设置数量
		//从session域中获取当前用户
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("sessionUser");
		cartItem.setUid(user.getUid());//设置用户ID
		
		cartService.addCartItem(cartItem);//调用业务层的添加方法
		return findByUid();//调用方法，重新查询购物车
	}
	/**
	 * 查询指定用户的购物车
	 * @return
	 */
	public String findByUid(){
		//从session域中获取当前用户
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("sessionUser");
		Integer uid = user.getUid();
		//通过用户ID查询当前用户的购物条目
		listCartItem = cartService.findByUid(uid);
		return "cartPage";//跳转到我的购物车页面
	}
	/**
	 * 移除购物车条目
	 * @return
	 */
	public String removeCartItemByPid(){
		//从session域中获取当前用户
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("sessionUser");
		Integer uid = user.getUid();
		cartService.removeCartItemByPid(uid,pid);//移除购物车条目
		return findByUid();//调用方法，重新查询购物车
	}
	/**
	 * 清空购物车
	 * @return
	 */
	public String clearCart(){
		//从session域中获取当前用户
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("sessionUser");
		Integer uid = user.getUid();
		cartService.clearCart(uid);
		return findByUid();//调用方法，重新查询购物车
	}
	/**
	 * 转到订单页面
	 * @return
	 */
	public String submitOrder(){
		//从session域中获取当前用户
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("sessionUser");
		Integer uid = user.getUid();
		listCartItem = cartService.findByUid(uid);
		return "orderPage";//跳转到订单页面
	}
	
	public List<CartItem> getListCartItem() {
		return listCartItem;
	}
	
	public void setPid(Integer pid) {
		this.pid = pid;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}
