package cn.itcast.shop.admin.product.action;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.admin.supplier.dao.AdminSupplierDao;
import cn.itcast.shop.admin.supplier.domain.Supplier;
import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.product.Service.ProductService;
import cn.itcast.shop.product.domain.Product;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台商品模块Action
 * @author 10467
 *
 */
public class AdminProductAction extends ActionSupport implements ModelDriven<Product>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3544644850825669189L;
	@Autowired
	private ProductService productService;
	@Autowired
	private AdminSupplierDao adminSupplierDao;
	private PageBean<Product> pageBean;
	private File upload;
	private String uploadFileName;
	//private String uploadContextType;
	
	
	public void setUpload(File upload) {
		this.upload = upload;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

//	public void setUploadContextType(String uploadContextType) {
//		this.uploadContextType = uploadContextType;
//	}

	public PageBean<Product> getPageBean() {
		return pageBean;
	}
	private int page;
	private List<Supplier> supplierList;
	/**
	 * 获取 supplierList
	 * @return the supplierList
	 */
	public List<Supplier> getSupplierList() {
		return supplierList;
	}

	/**
	 * 设置 supplierList
	 * @param supplierList the supplierList to set
	 */
	public void setSupplierList(List<Supplier> supplierList) {
		this.supplierList = supplierList;
	}

	/**
	 * 获取 serialversionuid
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setPage(int page) {
		this.page = page;
	}
	private Product product = new Product();
	@Override
	public Product getModel() {
		return product;
	}
	private List<CategorySecond> categorySecondList;
	public List<CategorySecond> getCategorySecondList() {
		return categorySecondList;
	}
	
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	
	public String findAll(){
		pageBean = productService.findAll(page);
		pageBean.setUrl(getUrl());
		return "listPage";
	}
	public String addPage(){
		categorySecondList = productService.findAllCategorySecond();
		supplierList = adminSupplierDao.findAllSupplier(0,100);
		return "addPage";
	}
	public String edit(){
		product = productService.findByPid(product.getPid());
		supplierList = adminSupplierDao.findAllSupplier(0,100);
		return "editPage";
	}
	public String save() throws IOException{
		product.setPdate(new Date());
		if(upload != null){
			//获得文件上传保存的磁盘绝对路径
			String realPath = ServletActionContext.getServletContext().getRealPath("/products");
			//创建文件
			File diskFile = new File(realPath+"//"+uploadFileName);
			//文件上传
			FileUtils.copyFile(upload, diskFile);
			product.setImage("products/"+uploadFileName);
		}
		productService.save(product);
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html;charset=UTF-8");
		resp.getWriter().println("<script>alert('添加商品成功！');location.href='${pageContext.request.contextPath}/adminProduct_findAll.action;'</script>");
		return NONE;
	}
	public String update() throws IOException{
		product.setPdate(new Date());
		if(upload != null){
			//获得文件上传保存的磁盘绝对路径
			String realPath = ServletActionContext.getServletContext().getRealPath("/products");
			//创建文件
			File diskFile = new File(realPath+"//"+uploadFileName);
			//文件上传
			FileUtils.copyFile(upload, diskFile);
			product.setImage("products/"+uploadFileName);
		}
		productService.update(product);
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html;charset=UTF-8");
		resp.getWriter().println("<script>alert('修改商品成功！');location.href='${pageContext.request.contextPath}/adminProduct_findAll.action;'</script>");
		return NONE;
	}
	public String delete() throws IOException{
		productService.delete(product);
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html;charset=UTF-8");
		resp.getWriter().println("<script>alert('删除商品成功！');location.href='${pageContext.request.contextPath}/adminProduct_findAll.action;'</script>");
		return NONE;
	}

	/**
	 * 获取 productService
	 * @return the productService
	 */
	public ProductService getProductService() {
		return productService;
	}

	/**
	 * 设置 productService
	 * @param productService the productService to set
	 */
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	/**
	 * 获取 adminSupplierDao
	 * @return the adminSupplierDao
	 */
	public AdminSupplierDao getAdminSupplierDao() {
		return adminSupplierDao;
	}

	/**
	 * 设置 adminSupplierDao
	 * @param adminSupplierDao the adminSupplierDao to set
	 */
	public void setAdminSupplierDao(AdminSupplierDao adminSupplierDao) {
		this.adminSupplierDao = adminSupplierDao;
	}

	/**
	 * 获取 product
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * 设置 product
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * 获取 upload
	 * @return the upload
	 */
	public File getUpload() {
		return upload;
	}

	/**
	 * 获取 uploadFileName
	 * @return the uploadFileName
	 */
	public String getUploadFileName() {
		return uploadFileName;
	}

	/**
	 * 获取 page
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * 设置 pageBean
	 * @param pageBean the pageBean to set
	 */
	public void setPageBean(PageBean<Product> pageBean) {
		this.pageBean = pageBean;
	}

	/**
	 * 设置 categorySecondList
	 * @param categorySecondList the categorySecondList to set
	 */
	public void setCategorySecondList(List<CategorySecond> categorySecondList) {
		this.categorySecondList = categorySecondList;
	}
	
}
