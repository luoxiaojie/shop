package cn.itcast.shop.admin.categorysecond.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.categorysecond.domain.CategorySecond;
import cn.itcast.shop.categorysecond.service.CategorySecondService;

/**
 * 后台处理二级分类模块的Action
 * @author 10467
 *
 */
public class AdminCategorySecondAction extends ActionSupport implements ModelDriven<CategorySecond>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2913141637492618258L;
	private CategorySecond categorySecond = new CategorySecond(); 
	@Override
	public CategorySecond getModel() {
		return categorySecond;
	}
	private List<Category> categoryList ;
	public List<Category> getCategoryList() {
		return categoryList;
	}
	@Autowired
	private CategorySecondService categorySecondService;
	
	public String addPage(){
		categoryList = categorySecondService.findAll();
		return "addPage";
	}
	public String edit(){
		categorySecond = categorySecondService.findByCsid(categorySecond.getCsid());
		return "editPage";
	}
	public String delete() throws IOException{
		categorySecond = categorySecondService.findByCsid(categorySecond.getCsid());
		if(categorySecond.getSetProduct()!=null && categorySecond.getSetProduct().size()>0){
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/html;charset=UTF-8");
			resp.getWriter().println("<script>alert('该二级分类下还有商品，不能删除！');location.href='${pageContext.request.contextPath}/adminCategory_findAll.action;'</script>");
			return NONE;
		}else{
			categorySecondService.delete(categorySecond);
			return "listPage";
		}
	}
	public String update(){
		categorySecondService.update(categorySecond);
		return "listPage";
	}
	public String save(){
		categorySecondService.save(categorySecond);
		return "listPage";
	}

}
