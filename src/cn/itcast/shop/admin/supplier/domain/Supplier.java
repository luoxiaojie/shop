package cn.itcast.shop.admin.supplier.domain;

/**
 * 后台供应商模块实体类
 * @author 10467
 *
 */
public class Supplier {
	private Integer sid; //供应商编号
	private String sname; //供应商名称
	private String linman;//联系人
	private String post;//联系人职务
	private String phone;//联系人电话
	private String email; //联系人邮件
	public Integer getSid() {
		return sid;
	}
	public void setSid(Integer sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getLinman() {
		return linman;
	}
	public void setLinman(String linman) {
		this.linman = linman;
	}
	public String getPost() {
		return post;
	}
	public void setPost(String post) {
		this.post = post;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
}
