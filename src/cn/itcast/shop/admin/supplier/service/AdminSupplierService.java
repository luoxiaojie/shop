package cn.itcast.shop.admin.supplier.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.admin.supplier.dao.AdminSupplierDao;
import cn.itcast.shop.admin.supplier.domain.Supplier;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台供应商模块业务逻辑层
 * @author 10467
 *
 */
@Service
public class AdminSupplierService {

	@Autowired
	private AdminSupplierDao adminSupplierDao;

	public PageBean<Supplier> findAllSupplier(int page) {
		PageBean<Supplier> pageBean = new PageBean<>();
		if(page==0)page=1;
		int limit = 10;
		pageBean.setLimit(limit);
		pageBean.setPage(page);
		
		int totalCount = adminSupplierDao.getSupplierCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Supplier> list = adminSupplierDao.findAllSupplier(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	public Supplier findBySid(Integer sid) {
		return adminSupplierDao.findBySid(sid);
	}

	public void delete(Integer sid) {
		adminSupplierDao.delete(sid);
	}

	public void update(Supplier supplier) {
		adminSupplierDao.update(supplier);
	}

	public void addSupplier(Supplier supplier) {
		adminSupplierDao.addSupplier(supplier);
	}

	
}
