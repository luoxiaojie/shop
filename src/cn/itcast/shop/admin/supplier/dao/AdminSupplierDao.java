package cn.itcast.shop.admin.supplier.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.admin.supplier.domain.Supplier;
/**
 * 后台供应商模块Dao
 * @author 10467
 *
 */
@Repository
@Transactional
public class AdminSupplierDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	public int getSupplierCount() {
		String hql = "select count(*) from Supplier ";
		int count = ((Long)getSession().createQuery(hql).uniqueResult()).intValue();
		return count;
	}

	public List<Supplier> findAllSupplier(int begin, int limit) {
		Query query = getSession().createQuery("from Supplier order by sid desc");
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Supplier> list = query.list();
		return list;
	}

	public Supplier findBySid(Integer sid) {
		Supplier supplier = (Supplier) getSession().get(Supplier.class, sid);
		return supplier;
	}

	public void delete(Integer sid) {
		Supplier supplier = (Supplier) getSession().get(Supplier.class, sid);
		getSession().delete(supplier);
	}

	public void update(Supplier supplier) {
		getSession().update(supplier);
	}

	public void addSupplier(Supplier supplier) {
		getSession().save(supplier);
	}

	

}
