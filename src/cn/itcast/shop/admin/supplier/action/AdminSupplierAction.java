package cn.itcast.shop.admin.supplier.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.admin.supplier.domain.Supplier;
import cn.itcast.shop.admin.supplier.service.AdminSupplierService;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台供应商模块Action
 * @author 10467
 *
 */
public class AdminSupplierAction extends ActionSupport implements ModelDriven<Supplier> {

	@Autowired
	private AdminSupplierService adminSupplierService;
	
	private static final long serialVersionUID = -4296018450696619512L;
	
	private Supplier supplier = new Supplier();
	@Override
	public Supplier getModel() {
		return supplier;
	}	
	private int page;
	public void setPage(int page) {
		this.page = page;
	}
	
	private PageBean<Supplier> pageBean;
	public PageBean<Supplier> getPageBean() {
		return pageBean;
	}
	
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	
	public String findAllSupplier(){
		pageBean = adminSupplierService.findAllSupplier(page);
		pageBean.setUrl(getUrl());
		return "listPage";
	}
	
	public String edit(){
		supplier = adminSupplierService.findBySid(supplier.getSid());
		return "editPage";
	}
	
	public String delete(){
		adminSupplierService.delete(supplier.getSid());
		return findAllSupplier();
	}
	
	public String update(){
		adminSupplierService.update(supplier);
		return edit();
	}
	public String addPage(){
		return "addPage";
	}
	public String addSupplier(){
		adminSupplierService.addSupplier(supplier);
		return findAllSupplier();
	}
}
