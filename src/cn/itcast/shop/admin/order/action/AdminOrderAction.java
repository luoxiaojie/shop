package cn.itcast.shop.admin.order.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.order.domain.OrderItem;
import cn.itcast.shop.order.domain.Orders;
import cn.itcast.shop.order.service.OrderService;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台订单模块的Action
 * @author 10467
 *
 */
public class AdminOrderAction extends ActionSupport implements ModelDriven<Orders>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8056262680533393059L;
	@Autowired
	private OrderService orderService;
	private Orders orders = new Orders();
	@Override
	public Orders getModel() {
		return orders;
	}
	private PageBean<Orders> pageBean;
	public PageBean<Orders> getPageBean() {
		return pageBean;
	}
	private int page;
	public void setPage(int page) {
		this.page = page;
	}
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	
	public String findAll(){
		pageBean = orderService.findAll(page);
		pageBean.setUrl(getUrl());
		return "listPage";
	}
	public String updateState() throws IOException{
		orderService.updateState(orders.getOid(), 3);
		orderService.createStock(orders.getOid());
		HttpServletResponse resp = ServletActionContext.getResponse();
		resp.setContentType("text/html;charset=UTF-8");
		resp.getWriter().println("<script>alert('发货成功！');location.href='${pageContext.request.contextPath}/adminOrder_findAll.action;'</script>");
		return NONE;
	}
	public String findOrderItem(){
		List<OrderItem> list = orderService.findOrderItem(orders.getOid());
		ActionContext.getContext().getValueStack().set("list",list);
		return "orderItemPage";
	}
	

}
