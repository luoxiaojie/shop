package cn.itcast.shop.admin.user.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.user.domain.User;
import cn.itcast.shop.user.service.UserService;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台用户模块Action
 * @author 10467
 *
 */
public class AdminUserAction extends ActionSupport implements ModelDriven<User>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3335045664669205651L;
	@Autowired
	private UserService userService;
	private User user = new User();
	private int page;
	public void setPage(int page) {
		this.page = page;
	}
	
	private PageBean<User> pageBean;
	public PageBean<User> getPageBean() {
		return pageBean;
	}
	
	@Override
	public User getModel() {
		return user;
	}
	
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	
	public String findAllUser(){
		pageBean = userService.findAllUser(page);
		pageBean.setUrl(getUrl());
		return "userListPage";
	}
	
	public String edit(){
		user = userService.findUserByUid(user.getUid());
		return "editPage";
	}
	
	public String delete(){
		userService.delete(user.getUid());
		return findAllUser();
	}
	
	public String update(){
		userService.update(user);
		return edit();
	}

	
	
}
