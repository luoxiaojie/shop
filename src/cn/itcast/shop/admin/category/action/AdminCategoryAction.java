package cn.itcast.shop.admin.category.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.category.service.CategoryService;

/**
 * 后台分类模块的Action
 * @author 10467
 *
 */
public class AdminCategoryAction extends ActionSupport implements ModelDriven<Category>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8924339036207377155L;
	@Autowired
	private CategoryService categoryService;
	private List<Category> categoryList;
	public List<Category> getCategoryList() {
		return categoryList;
	}
	private Category category = new Category();
	@Override
	public Category getModel() {
		return category;
	}
	
	public String findAll(){
		categoryList = categoryService.findAll();
		return "listPage";
	}
	public String addPage(){
		return "addPage";
	}
	public String edit(){
		category = categoryService.findByCid(category.getCid());
		return "editPage";
	}
	public String update(){
		categoryService.update(category);
		return findAll();
	}
	public String delete() throws IOException{
		category = categoryService.findByCid(category.getCid());
		if(category.getSetCategorySecond().size()>0&&category.getSetCategorySecond()!=null){
			HttpServletResponse resp = ServletActionContext.getResponse();
			resp.setContentType("text/html;charset=UTF-8");
			resp.getWriter().println("<script>alert('该分类下还有二级分类，不能删除！');location.href='${pageContext.request.contextPath}/adminCategory_findAll.action;'</script>");
			return NONE;
		}else{
			categoryService.delete(category);
			return findAll();
		}
	}
	public String save(){
		categoryService.save(category);
		return findAll();
	}

	
}
