package cn.itcast.shop.admin.stock.domain;

import java.util.Date;

import cn.itcast.shop.order.domain.OrderItem;
/**
 * 后台仓库模块实体类
 * @author 10467
 *
 */
public class Stock {
	private Integer osid; //出库编号
	private Integer num;//0为入库，1为出库
	private Date tockTime;//时间
	private String username;//所属客户
	private OrderItem orderItem;//订单项
	private String note;//备注
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Integer getOsid() {
		return osid;
	}
	public void setOsid(Integer osid) {
		this.osid = osid;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Date getTockTime() {
		return tockTime;
	}
	public void setTockTime(Date tockTime) {
		this.tockTime = tockTime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public OrderItem getOrderItem() {
		return orderItem;
	}
	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}
	
	
	
	
	
}
