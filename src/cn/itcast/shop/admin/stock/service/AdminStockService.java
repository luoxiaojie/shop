package cn.itcast.shop.admin.stock.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.admin.stock.dao.AdminStockDao;
import cn.itcast.shop.admin.stock.domain.Stock;
import cn.itcast.shop.utils.pagebean.PageBean;
/**
 * 后台仓库模块业务逻辑层
 * @author 10467
 *
 */
@Service
public class AdminStockService {
	
	@Autowired
	private AdminStockDao adminStockDao;

	public PageBean<Stock> findAllOutStock(int page) {
		PageBean<Stock> pageBean = new PageBean<>();
		if(page==0)page=1;
		int limit = 10;
		pageBean.setLimit(limit);
		pageBean.setPage(page);
		
		int totalCount = adminStockDao.getOutStockCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Stock> list = adminStockDao.findAllOutStock(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	public PageBean<Stock> findAllInStock(int page) {
		PageBean<Stock> pageBean = new PageBean<>();
		if(page==0)page=1;
		int limit = 10;
		pageBean.setLimit(limit);
		pageBean.setPage(page);
		
		int totalCount = adminStockDao.getInStockCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Stock> list = adminStockDao.findAllInStock(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}
	
}
