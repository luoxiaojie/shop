package cn.itcast.shop.admin.stock.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.shop.admin.stock.domain.Stock;
/**
 * 后台仓库模块Dao
 * @author 10467
 *
 */
@Repository
@Transactional
public class AdminStockDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	public int getOutStockCount() {
		String hql = "select count(*) from Stock where num=1 ";
		int count = ((Long)getSession().createQuery(hql).uniqueResult()).intValue();
		return count;
	}

	public List<Stock> findAllOutStock(int begin, int limit) {
		Query query = getSession().createQuery("from Stock where num=1 order by osid desc");
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Stock> list = query.list();
		return list;
	}

	public int getInStockCount() {
		String hql = "select count(*) from Stock where num=0 order by osid desc ";
		int count = ((Long)getSession().createQuery(hql).uniqueResult()).intValue();
		return count;
	}

	public List<Stock> findAllInStock(int begin, int limit) {
		Query query = getSession().createQuery("from Stock where num=0 ");
		query.setFirstResult(begin);
		query.setMaxResults(limit);
		@SuppressWarnings("unchecked")
		List<Stock> list = query.list();
		return list;
	}
	
}
