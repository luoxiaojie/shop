package cn.itcast.shop.admin.stock.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.admin.stock.domain.Stock;
import cn.itcast.shop.admin.stock.service.AdminStockService;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 后台仓库模块Action
 * @author 10467
 *
 */
public class AdminStockAction extends ActionSupport implements ModelDriven<Stock>{

	private static final long serialVersionUID = -7238305052631720532L;
	
	@Autowired
	private AdminStockService adminStockService;
	
	private Stock stock = new Stock();
	@Override
	public Stock getModel() {
		return stock;
	}
	
	private int page;
	public void setPage(int page) {
		this.page = page;
	}
	
	private PageBean<Stock> pageBean;
	public PageBean<Stock> getPageBean() {
		return pageBean;
	}
	
	private String getUrl() {
		//等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI()+"?"+req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if(index != -1) {
			url = url.substring(0,index);
		}
		return url ;  
	}
	
	public String findAllOutStock(){
		pageBean = adminStockService.findAllOutStock(page);
		pageBean.setUrl(getUrl());
		return "outListPage";
	}
	public String findAllInStock(){
		pageBean = adminStockService.findAllInStock(page);
		pageBean.setUrl(getUrl());
		return "inListPage";
	}
	
}
