package cn.itcast.shop.admin.admin.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.shop.admin.admin.domain.Admin;
import cn.itcast.shop.admin.admin.service.AdminService;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 
* @ClassName: AdminAction  
* @Description: 管理员模块的Action
* @author:HuangFeng  
* @date:2017年3月5日下午8:36:16
*
 */
public class AdminAction extends ActionSupport implements ModelDriven<Admin> {

	private static final long serialVersionUID = -6227479245063370401L;

	@Autowired
	private AdminService adminService;

	private Admin admin = new Admin();

	/**
	 * (non-Javadoc)   
	* @see com.opensymphony.xwork2.ModelDriven#getModel()
	 */
	@Override
	public Admin getModel() {
		return admin;
	}

	private int page;

	/**
	 * 
	 * @Title: setPage
	 * @Description:
	 * @param page 
	 * @return: void
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:39:20
	 */
	public void setPage(int page) {
		this.page = page;
	}

	private PageBean<Admin> pageBean;

	/**
	 * 
	 * @Title: getPageBean
	 * @Description:
	 * @return 
	 * @return: PageBean<Admin>
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:39:49
	 */
	public PageBean<Admin> getPageBean() {
		return pageBean;
	}

	/**
	 * 
	 * @Title: getUrl
	 * @Description:
	 * @return 
	 * @return: String
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:40:22
	 */
	private String getUrl() {
		// 等于 /项目名/Servlet路径?method=find&pc=3
		HttpServletRequest req = ServletActionContext.getRequest();
		String url = req.getRequestURI() + "?" + req.getQueryString();
		int index = url.lastIndexOf("&page=");
		if (index != -1) {
			url = url.substring(0, index);
		}
		return url;
	}

	/**
	 * 查询所有管理员
	 * 
	 * @return
	 */
	public String findAllAdmin() {
		pageBean = adminService.findAllAdmin(page);
		pageBean.setUrl(getUrl());
		return "adminListPage";
	}

	/**
	 * 跳转到登陆页
	 * 
	 * @return
	 */
	public String index() {
		return "adminLoginPage";
	}

	public String top() {
		return "topPage";
	}

	public String left() {
		return "leftPage";
	}

	public String welcome() {
		return "welcomePage";
	}

	/**
	 * 
	 * @Title: login
	 * @Description:
	 * @return 
	 * @return: String
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:40:35
	 */
	public String login() {
		HttpServletRequest req = ServletActionContext.getRequest();
		String username = admin.getUsername();
		if (username == null || username.trim().isEmpty()) {
			req.setAttribute("msg", "管理员姓名不能为空！");
			return "adminLoginPage";
		}
		// 密码校验
		String password = admin.getPassword();
		if (password == null || password.trim().isEmpty()) {
			req.setAttribute("msg", "密码不能为空！");
			return "adminLoginPage";
		} else if (!password.matches("\\w{5,16}$")) {
			req.setAttribute("msg", "管理员姓名或密码错误！");
			return "adminLoginPage";
		}

		Admin user = adminService.login(admin);
		if (user != null) {
			req.getSession().setAttribute("sessionAdmin", user);
			return "homePage";
		} else {
			req.setAttribute("msg", "管理员姓名或密码错误！");
			return "adminLoginPage";
		}

	}

	/**
	 * 管理员退出
	 * 
	 * @return
	 */
	public String exit() {
		ServletActionContext.getRequest().getSession().invalidate();
		return "adminLoginPage";
	}

	/**
	 * 删除管理员
	 * 
	 * @return
	 */
	public String delete() {
		adminService.delete(admin.getUid());
		return findAllAdmin();
	}
}
