package cn.itcast.shop.admin.admin.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.admin.admin.dao.AdminDao;
import cn.itcast.shop.admin.admin.domain.Admin;
import cn.itcast.shop.utils.pagebean.PageBean;

/**
 * 管理员模块的业务层
 * @author 10467
 *
 */
@Service
public class AdminService {
	
	@Autowired
	private AdminDao adminDao;
	/**
	 * 登陆方法
	 * @param admin
	 * @return
	 */
	public Admin login(Admin admin) {
		return adminDao.login(admin.getUsername(),admin.getPassword());
	}
	/**
	 * 查询所有管理员
	 * @param page
	 * @return
	 */
	public PageBean<Admin> findAllAdmin(int page) {
		PageBean<Admin> pageBean = new PageBean<>();
		if(page==0)page=1;
		int limit = 10;
		pageBean.setLimit(limit);
		pageBean.setPage(page);
		
		int totalCount = adminDao.getAdminCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Admin> list = adminDao.findAllAdmin(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}
	/**
	 * 删除管理员
	 * @param uid
	 */
	public void delete(Integer uid) {
		adminDao.delete(uid);
	}

}
