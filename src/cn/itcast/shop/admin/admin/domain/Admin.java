package cn.itcast.shop.admin.admin.domain;

/**
 * 管理员的实体类
 * @author 10467
 *
 */
public class Admin {
	private Integer uid;
	private String username;
	private String password;
	public Integer getUid() {
		return uid;
	}
	/**
	 * 
	 * @Title: setUid
	 * @Description:
	 * @param uid 
	 * @return: void
	 * @author：HuangFeng
	 * @throws
	 * @date：2017年3月5日 下午8:41:01
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
