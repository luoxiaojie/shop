package cn.itcast.shop.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import cn.itcast.shop.admin.admin.domain.Admin;

/**
 * 后台拦截器
 */
public class AdminLoginInterceptor extends MethodFilterInterceptor {

	private static final long serialVersionUID = -8641545730295726489L;

	/**
	 * 拦截方法
	 */
	@Override
	protected String doIntercept(ActionInvocation arg0) throws Exception {
		//获得请求对象
		HttpServletRequest req = ServletActionContext.getRequest();
		//从Session中获取管理员信息
		Admin user = (Admin) req.getSession().getAttribute("sessionAdmin");
		if (user == null) {
			//如果没有获取到管理员，说明没有权限
			req.setAttribute("msg", "你还没有登录，请登录");
			//拦截请求，直接转到错误信息页面
			return "adminLoginPage";
		} else {
			//不拦截请求，放行
			return arg0.invoke();
		}
	}

}
