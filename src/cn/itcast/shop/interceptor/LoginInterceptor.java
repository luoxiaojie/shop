package cn.itcast.shop.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import cn.itcast.shop.user.domain.User;

/**
 * 前台页面拦截器
 */
public class LoginInterceptor extends MethodFilterInterceptor {

	private static final long serialVersionUID = 220220120620035064L;

	/**
	 * 拦截方法
	 */
	@Override
	protected String doIntercept(ActionInvocation arg0) throws Exception {
		//获得当前请求对象
		HttpServletRequest req = ServletActionContext.getRequest();
		//从session中获取用户信息
		User user = (User) req.getSession().getAttribute("sessionUser");
		if(user == null){
			//如果没有获取到，说明还没有登录，没有权限
			req.setAttribute("code", "error");
			req.setAttribute("msg", "你还没有登录，请登录");
			//拦截请求，直接转到错误信息页面
			return "msg";
		}else{
			//不拦截请求，放行
			return arg0.invoke();
		}
	}

}
