package cn.itcast.shop.order.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import cn.itcast.shop.user.domain.User;
/**
 * 订单实体类
 * @author 10467
 *
 */
public class Orders {
	
	private String oid;
	private Double total;//总计
	private Date ordertime;//下单时间
	private Integer state;//订单状态
	private String name;//收货人
	private String phone;//电话
	private String addr;//收货地址
	private User user;//用户
	
	private Set<OrderItem> setOrderItem = new HashSet<>();//订单项
	
	
	public Set<OrderItem> getSetOrderItem() {
		return setOrderItem;
	}
	public void setSetOrderItem(Set<OrderItem> setOrderItem) {
		this.setOrderItem = setOrderItem;
	}
	
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Date getOrdertime() {
		return ordertime;
	}
	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

}
