package cn.itcast.shop.order.dao;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import cn.itcast.shop.order.domain.OrderItem;
import cn.itcast.shop.order.domain.Orders;
import cn.itcast.shop.order.exception.OrderException;

public interface OrderDao {

	Set<OrderItem> loadOrderItem(String cartItemIds) throws OrderException;

	void createOrder(Orders orders);

	int findCountByUid(Integer uid);

	List<Orders> findByUid(Integer uid, int begin, int limit);

	Orders findByOid(String oid);

	void updateState(String oid, int i);

	List<Orders> findAll(int begin , int limit );

	int findAllCount();

	List<OrderItem> findOrderItem(String oid);

	void createStock(String oid);

	void cancelOrder(String oid);

	List<Orders> findThisMonthZD(Integer uid,int begin ,int limit ,String zddate) throws ParseException;

	int findCountByDate(Integer uid, String zddate) throws ParseException;

}
