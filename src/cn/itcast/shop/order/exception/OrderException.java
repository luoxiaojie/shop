package cn.itcast.shop.order.exception;
/**
 * 自定义订单异常
 * @author 10467
 *
 */
public class OrderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1871668632572162906L;

	public OrderException() {
		super();
	}

	public OrderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public OrderException(String message, Throwable cause) {
		super(message, cause);
	}

	public OrderException(String message) {
		super(message);
	}

	public OrderException(Throwable cause) {
		super(cause);
	}

	
	
}
