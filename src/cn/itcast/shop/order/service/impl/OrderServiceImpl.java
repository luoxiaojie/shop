package cn.itcast.shop.order.service.impl;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.itcast.shop.order.dao.OrderDao;
import cn.itcast.shop.order.domain.OrderItem;
import cn.itcast.shop.order.domain.Orders;
import cn.itcast.shop.order.exception.OrderException;
import cn.itcast.shop.order.service.OrderService;
import cn.itcast.shop.utils.pagebean.PageBean;
/**
 * 订单模块业务逻辑层
 * @author 10467
 *
 */
@Service("orderService")
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDao orderDao;
	
	/**
	 * 加载订单项
	 */
	@Override
	public Set<OrderItem> loadOrderItem(String cartItemIds) throws OrderException{
		 return orderDao.loadOrderItem(cartItemIds);
	}

	@Override
	public Orders createOrder(Orders orders) {
		orderDao.createOrder(orders);
		return orders;
	}
	
	@Override
	public PageBean<Orders> findByUid(Integer uid, int page) {
		PageBean<Orders> pageBean = new PageBean<>();
		int limit = 12;
		pageBean.setLimit(limit);
		if(page==0) page=1;
		pageBean.setPage(page);
		
		int totalCount =orderDao.findCountByUid(uid);
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Orders> list = orderDao.findByUid(uid,begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public Orders findByOid(String oid) {
		return orderDao.findByOid(oid);
	}

	@Override
	public void updateState(String oid, int i) {
		orderDao.updateState(oid,i);
		orderDao.cancelOrder(oid);
	}

	@Override
	public PageBean<Orders> findAll(int page) {
		PageBean<Orders> pageBean = new PageBean<>();
		int limit = 20;
		pageBean.setLimit(limit);
		if(page==0)page=1;
		pageBean.setPage(page);
		
		int totalCount =orderDao.findAllCount();
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Orders> list = orderDao.findAll(begin,limit);
		pageBean.setList(list);
		return pageBean;
	}

	@Override
	public List<OrderItem> findOrderItem(String oid) {
		return orderDao.findOrderItem(oid);
	}

	@Override
	public void createStock(String oid) {
		orderDao.createStock(oid);
	}

	@Override
	public PageBean<Orders> findThisMonthZD(Integer uid, int page ,String zddate) throws ParseException {
		PageBean<Orders> pageBean = new PageBean<>();
		int limit = 20;
		pageBean.setLimit(limit);
		if(page==0)page=1;
		pageBean.setPage(page);
		
		int totalCount =orderDao.findCountByDate(uid,zddate);
		pageBean.setTotalCount(totalCount);
		
		int begin = (page-1)*limit;
		List<Orders> list = orderDao.findThisMonthZD(uid,begin,limit,zddate);
		pageBean.setList(list);
		return pageBean;
	}
	
	
}
