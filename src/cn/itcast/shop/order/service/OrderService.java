package cn.itcast.shop.order.service;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

import cn.itcast.shop.order.domain.OrderItem;
import cn.itcast.shop.order.domain.Orders;
import cn.itcast.shop.order.exception.OrderException;
import cn.itcast.shop.utils.pagebean.PageBean;

public interface OrderService {

	Set<OrderItem> loadOrderItem(String cartItemIds) throws OrderException;

	Orders createOrder(Orders orders);

	PageBean<Orders> findByUid(Integer uid, int page);

	Orders findByOid(String oid);

	void updateState(String oid, int i);

	PageBean<Orders> findAll(int page);

	List<OrderItem> findOrderItem(String oid);

	void createStock(String oid);

	PageBean<Orders> findThisMonthZD(Integer uid, int page ,String zddate) throws ParseException;

}
