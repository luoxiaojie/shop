package cn.itcast.shop.utils.pagebean;

import java.util.List;

/**
 * 分页对象的实体类
 */
public class PageBean<T> {

	/**
	 * 页数
	 */
	private int page;

	/**
	 * 总记录数
	 */
	private int totalCount;

	/**
	 * 每页显示的条数
	 */
	private int limit;

	/**
	 * 分页http路径
	 */
	private String url;

	/**
	 * 分页装载的集合
	 */
	private List<T> list;

	/**
	 * 获得总页数
	 */
	public int getTotalPage() {
		int totalPage = totalCount / limit;//总记录数除以每页显示的条数
		//如果有余数就把总页数加1
		return totalCount % limit == 0 ? totalPage : totalPage + 1;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
