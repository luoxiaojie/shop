package cn.itcast.shop.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonthDate {
	static  Calendar cale = null; 
	
	public static Date thisMonthFirthday(String nowdate) throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date parse = simpleDateFormat.parse(nowdate);
		
		cale = Calendar.getInstance(); 
		cale.setTime(parse);
        cale.add(Calendar.MONTH, 0);  
        cale.set(Calendar.DAY_OF_MONTH, 1);  
        return cale.getTime();
	}
	
	public static Date thisMonthLastday(String nowdate) throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date parse = simpleDateFormat.parse(nowdate);
		cale = Calendar.getInstance(); 
		cale.setTime(parse);
        cale.add(Calendar.MONTH, 1);  
        cale.set(Calendar.DAY_OF_MONTH, 0); 
        return cale.getTime();
	}
}
