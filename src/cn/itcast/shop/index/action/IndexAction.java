package cn.itcast.shop.index.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import cn.itcast.shop.category.domain.Category;
import cn.itcast.shop.category.service.CategoryService;
import cn.itcast.shop.product.Service.ProductService;
import cn.itcast.shop.product.domain.Product;

/**
 * 访问首页的Action
 */
public class IndexAction extends ActionSupport{
	
	private static final long serialVersionUID = 1025901015421050974L;

	/**
	 * 注入分类模块的业务层对象
	 */
	@Autowired
	private CategoryService categoryService;
	
	/**
	 * 注入商品的业务对象
	 */
	@Autowired
	private ProductService productService;
	
	/**
	 * 访问Action时，默认访问此方法
	 */
	public String execute(){
		List<Category> cList = categoryService.findAll();//查询所有分类
		ActionContext.getContext().getSession().put("cList", cList);//将分类信息存入session域中
		
		List<Product> hList = productService.findHot();//查询热门商品
		ActionContext.getContext().getValueStack().set("hList",hList);//将商品信息存入Struts2的值栈中
		
		List<Product> nList = productService.findNew();//查询最新商品
		ActionContext.getContext().getValueStack().set("nList",nList);//将商品信息存入Struts2的值栈中
		return "indexPage";//返回到主页
	}
}
